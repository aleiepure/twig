


---
title: "#166 Forty-seven!"
author: Felix
date: 2024-09-20
tags: ["gnome-calendar", "eyedropper", "gnome-mahjongg", "libadwaita", "glib", "loupe", "gnome-shell"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 13 to September 20.<!--more-->

**This week we released GNOME 47!**

This new major release of GNOME is full of exciting changes, including accent colours, better open/save dialogs, an improved Files app, better support for small screen sizes, new dialog styles, and much more! See the [GNOME 47 release notes](https://release.gnome.org/47/) and [developer notes](https://release.gnome.org/47/developers/index.html) for more information. 

Readers who have been following this site will already be aware of some of the new features. If you'd like to follow the development of GNOME 48 (Spring 2025), keep an eye on this page - we'll be posting exciting news every week!

{{< youtube sgcVp5RHy4Q >}}

# GNOME Core Apps and Libraries

### Image Viewer (Loupe) [↗](https://apps.gnome.org/app/org.gnome.Loupe/)

Browse through images and inspect their metadata.

[Sophie 🏳️‍🌈 🏳️‍⚧️ (she/her)](https://matrix.to/#/@sophieherold:gnome.org) says

> The changes for Loupe 47 have mostly been subtle and in the background. But for Loupe 48, we are already full steam ahead of making a lot of more noticeable changes, including work on image editing. You can learn more in my latest [blog post](https://blogs.gnome.org/sophieh/2024/09/20/image-viewing-and-editing-in-gnome-47-and-beyond/) or even get weekly updates as a backer on [Patreon](https://www.patreon.com/sophieh) or [Ko-fi](https://ko-fi.com/sophieherold).
> ![](9314391bca7d361c1d7fa2433966c316220551341837209901051412480.png)

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) announces

> the new development cycle has started, so libadwaita now has [toggle groups](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.ToggleGroup.html) as a replacement for linked boxes of exclusive toggle buttons. Having a dedicated widget not only provides easier to use API, but also uses a less ambiguous style that wouldn't be possible with a generic box.
> 
> There's also an [inline view switcher](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.InlineViewSwitcher.html) using a toggle group. It works with `AdwViewStack` instead of `GtkStack`, and so `AdwViewStack` has an optional crossfade transition now, as it's commonly needed in contexts where inline view switchers are used.
> 
> Meanwhile, the bottom bar in `AdwBottomSheet` can now be [hidden](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/property.BottomSheet.reveal-bottom-bar.html), which may be useful for empty states in music players
> 
> Additionally, James Westman added [a property](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1264) to add banner to a preferences page, while Emmanuele Bassi added a few [cubic bezier easing functions](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/enum.Easing.html#ease) for `AdwTimedAnimation`
> ![](42fa2a25fb1970816acf4640e6efc49907ac53c81836764619390058496.png)

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Julian Sparber (he/him)](https://matrix.to/#/@jsparber:gnome.org) announces

> I wrote, as part of the STF grant, a [blogpost](https://blogs.gnome.org/shell-dev/2024/09/20/understanding-gnome-shells-focus-stealing-prevention/) about focus stealing prevention in GNOME Shell.

### Calendar [↗](https://gitlab.gnome.org/GNOME/gnome-calendar/)

A simple calendar application.

[Hari Rana | TheEvilSkeleton](https://matrix.to/#/@theevilskeleton:fedora.im) says

> GNOME Calendar 47.0 was just [released](https://gitlab.gnome.org/GNOME/gnome-calendar/-/tree/47.0?ref_type=tags)! There have been many improvements in regards to usability:
> 
> * It is now possible to [drag and drop ICS files into Calendar](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/471).
> * Port various widgets to their modern Adwaita equivalents: [!436](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/436), [!481](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/481)
> * [Hidden calendars are distinguishable](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/477) by using the `eye-not-looking` icon.
> * [The search activation was delayed](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/414). This avoids spamming the search backends, which improves the overall performance, reduces power consumption, and eliminates flickering in the interface.
> * Various fixes in the agenda sidebar: [!374](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/374), [!445](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/445), [!464](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/464), [!468](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/468)
> * The Event popover was [reworked and redesigned](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/392). It introduces a padlock icon for read-only events, properly separates each section, and it wraps/ellipsizes text properly.
> * The about dialog was [ported](https://gitlab.gnome.org/GNOME/gnome-calendar/-/merge_requests/430) to [`AdwAboutDialog.new_from_appdata`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/ctor.AboutDialog.new_from_appdata.html). This makes it easy for us to include release notes without putting any effort. This means, starting from 47, it will now be easy to view release notes directly in Calendar.
> ![](d316dc7b91904648621fafefe89834e52fab547a1835697631481823232.png)
> ![](734f055990d63cf96bc9bbea37532135cb5bbdb21835698115181543424.png)
> ![](e1c19a4aab1c8b01081a9107b4c5c00435a707b61835697591342333952.png)

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Gleb Popov has contributed a new file monitor for BSD and macOS to GLib, which uses libinotify-kqueue: https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3657

# GNOME Circle Apps and Libraries

[Gregor Niehl](https://matrix.to/#/@gregorni:gnome.org) announces

> This week [Binary](https://apps.gnome.org/Binary/) was accepted into GNOME Circle. Binary makes working with numbers of different bases (e.g. binary, hexadecimal) a breeze. Congratulations!
> ![](eff652beb2938cbab90948d434a5c8ec465b5aea1835429600792936449.png)

### Eyedropper [↗](https://flathub.org/apps/details/com.github.finefindus.eyedropper)

Pick and format colors.

[FineFindus](https://matrix.to/#/@finefindus:matrix.org) announces

> After almost a year since the last update, the new version of Eyedropper brings significant changes both to the user interface and under the hood:
> * A new color editor allows modifying the picked color
>  - The color system has been completely rewritten, now using the [palette library](https://github.com/Ogeon/palette/) for more accurate color conversions.
>  - If the system does not allow color picking, a warning page is now displayed (Thanks to Brage Fuglseth for the illustration)
> 
> Download the new version on [Flathub](https://flathub.org/apps/com.github.finefindus.eyedropper).
> ![](NmQTsyupXmUEAhrVQgjNFntg.png)
> ![](IxYAbXffAModomxcJNwwpMHJ.png)

# Third Party Projects

[tfuxu](https://matrix.to/#/@tfuxu:matrix.org) reports

> The first version of [Flood It](https://github.com/tfuxu/floodit) has been released! It is a simple strategy game in which you need to flood the entire board with a single color in as few moves as possible.
> 
> Check it out on [Flathub](https://flathub.org/apps/io.github.tfuxu.floodit)!
> ![](KppgWJDbZeiCQJeuceoxOUnM.png)
> ![](jASCpMGdKGXKctnjOqzpEZRT.png)

[Ronnie Nissan](https://matrix.to/#/@ronniedroid:mozilla.org) says

> Embellish has been ported from Go to Gjs, now with a lot smaller bundle size and new features:
> * Font previews
> * Font licences
> * A welcome screen
> 
> You can get the latest embellish on [flathub](https://flathub.org/apps/io.github.getnf.embellish)
> ![](97de7fce7a54e04bd174f5b9a1fba2254fa909611837016658569330688.jpg)

### Mahjongg [↗](https://gitlab.gnome.org/GNOME/gnome-mahjongg)

A solitaire version of the classic Eastern tile game.

[Mat](https://matrix.to/#/@mat:mathias.is) says

> Mahjongg 47.0 has been released, and is [available on Flathub](https://flathub.org/apps/org.gnome.Mahjongg). This release contains several improvements, including:
> 
> * Changes to the UI to follow the latest GNOME Human Interface Guidelines
> * Player names for scores, and the ability to clear all previous scores
> * Faster game startup times
> * A fix for unclickable tiles in the 'Overpass' layout
> ![](VfBkKTRlkYpSgRwlvGwpkcRL.png)
> ![](PakqnzazRnZGJthGGgjIHcKX.png)

# GNOME Foundation

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) says

> Linux App Summit 2024 is two weeks away! This year’s conference will take place on Oct 4-5 in Monterrey, Mexico and all main track talks will be live-streamed for remote attendees. Registration is still open for both [in-person](https://conf.linuxappsummit.org/event/6/registrations/16/) and [remote](https://conf.linuxappsummit.org/event/6/registrations/15/) attendance, make sure to let us know how you plan to attend. More event details including the full talk schedule can be found on [linuxappsummit.org](https://linuxappsummit.org/).
> 
> The GNOME Asia 2024 Call for Participation is still open! If you would like to submit a talk or workshop for this year’s summit make sure to [apply online](https://events.gnome.org/event/258/abstracts/#submit-abstract) by September 30. This year’s conference will take place in Bengaluru, India from Dec 6-8 and allow attendees and speakers to participate remotely. [Learn more about GNOME Asia 2024](https://foundation.gnome.org/2024/09/12/gnome-asia-2024-in-bengaluru-india/).
> 
> The GNOME Foundation is searching for applicants for our open Executive Director position. We’ve extended the application deadline until September 25 and encourage qualified individuals who share our vision of promoting software freedom and innovation to apply. Learn more about the position and how to apply [here](https://foundation.gnome.org/2024/09/13/search-for-new-executive-director/).

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

