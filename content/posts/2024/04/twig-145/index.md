---
title: "#145 Quality Over Quantity"
author: Felix
date: 2024-04-26
tags: ["graphs", "gtk"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from April 19 to April 26.<!--more-->

# GNOME Core Apps and Libraries

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Matthias Clasen](https://matrix.to/#/@matthiasc:gnome.org) says

> GTK 4.15.0 is out.
> 
> This release changes the default GSK renderer to be Vulkan, on Wayland. Other platforms still use ngl.
> 
> The intent of this change is to get wider testing and verify that Vulkan drivers are good enough for us to rely on. If significant problems show up, we will revert this change for 4.16.
> 
> You can still override the renderer choice using the GSK\_RENDERER environment variable.
> 
> This release also changes font rendering settings by introducing a new high-level gtk-font-rendering settings which gives GTK more freedom to decide on font rendering.
> 
> You can still use the low-level font-related settings by changing the new property to 'manual'.
> 
> If you are building GTK for a distribution, notice that some deprecated build options have been removed.

# GNOME Circle Apps and Libraries

[FineFindus](https://matrix.to/#/@finefindus:matrix.org) reports

> I've released a new version of Hieroglyphic. This version features a new app icon by Tobias Bernard, refines the UI and improves the symbol recognition speed. You can download it from [Flathub](https://flathub.org/apps/io.github.finefindus.Hieroglyphic).
> ![](pZSOyuDlhJgquNxHlyJWqjtu.png)

### Graphs [↗](https://graphs.sjoerd.se)

Plot and manipulate data

[Sjoerd Stendahl](https://matrix.to/#/@sjoerdb93:matrix.org) reports

> This week we released version 1.8 of Graphs. This release mostly focuses on background changes to the code, but there's still a number of nice quality of life changes that are interesting to highlight:
> 
> * Graphs now has full support for touchscreen devices
> * Equation handling has been improved. Now it is no longer needed to add a `*` symbol between every single parameter upon multiplication, instead an equation such as `y = 4sin(2x + pi)` is handled without any issues.
> * Support has been added for additional trigonometric functions and their inverses, such as `sinh` and `cosh`. Now the vast majority of trig functions should work without any issues.
> * Help has been ported to Yelp, and can now be accesed directly from the app. This documentation is still a work in progress and will be expanded upon throughout this release cycle.
> * There is now a warning when editing a style with poor contrast between the labels and background colours.
> * Numerical entries throughout the application now indicate whenever an invalid input is given.
> * Multiple bug fixes were implemented, especially with regard to the equation handling in the curve fitting dialog.
> * Many different improvements have been added to the translation process such as more translatable strings, and added context to strings. Thanks for the feedback from the translation team :)
> 
> As always, the latest release can be found on [Flathub](https://flathub.org/apps/details/se.sjoerd.Graphs). For any feedback, suggestions or bug reports, please file an issue at the [GitLab issue tracker](https://gitlab.gnome.org/World/Graphs/-/issues/).
> ![](wEOjJqThEviNQZVdoXDVyJsR.png)
> ![](MOJRlxgsKENPNTqyFoaLgNFn.png)
> {{< video src="LrxmSYKljSygwAhLiLvFadAW.webm" >}}

# GNOME Foundation

[ramcq](https://matrix.to/#/@ramcq:matrix.org) announces

> The board of the GNOME Foundation is pleased to [share an update](https://discourse.gnome.org/t/update-from-the-board/20653) about finances, strategy, fundraising, and our exciting plans to bring more diverse and inclusive representation into how the Foundation is governed and can best support and promote the GNOME community. We're adding two seats to the board from the upcoming election which starts next month, and are interested to speak with prospective candidates about what's involved and how they can help.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!