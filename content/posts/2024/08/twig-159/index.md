---
title: "#159 Mounting Disks"
author: Felix
date: 2024-08-02
tags: ["epiphany", "gaphor", "fractal"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 26 to August 02.<!--more-->

# GNOME Core Apps and Libraries

[FineFindus](https://matrix.to/#/@finefindus:matrix.org) says

> GNOME Disks now has a new standalone image-mounter dialog, which allows for mounting, viewing, editing, writing, and inspecting disk images. If the disk image is already mounted, an option to unmount it will be available. The dialog can be accessed by opening any supported disk image. Together with the GTK4 port this is expected to be released as part of GNOME 48.
> {{< video src="xbtHYHWVfvLGBXPUTuqNNltS.webm" >}}

### Web [↗](https://gitlab.gnome.org/GNOME/epiphany)

Web browser for the GNOME desktop.

[Michael Catanzaro](https://matrix.to/#/@mcatanzaro:gnome.org) announces

> Unfortunately, due to changes in the Firefox account authentication process, we have disabled the Firefox Sync support in Epiphany 47.beta, 46.3, and 45.4. We don't have any estimate for when Firefox Sync will return. For more information see [this issue report.](https://gitlab.gnome.org/GNOME/epiphany/-/issues/2337)

[Michael Catanzaro](https://matrix.to/#/@mcatanzaro:gnome.org) announces

> Jan-Michael Brummer, with guidance from Jamie Gravendeel, has replaced the Epiphany bookmarks management popover with a sidebar. Additionally, the popover for adding a new bookmark has been replaced with a dialog.
> ![](ce228d9be0eadb2e837e6629fc50a49240f0003b1819468516579868672.png)

# GNOME Circle Apps and Libraries

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) reports

> I created a short post, outlining how we [upgraded Gaphor from GTK+ 3 to GTK 4](https://gaphor.org/2024/07/30/gtk-upgrade-in-python/). I hope it will help people in the process of upgrading their app.

# Third Party Projects

[Izzy (she/her)](https://matrix.to/#/@fizzyizzy05:matrix.org) reports

> Binary 4.0 is out now, featuring the new vertical design that is easier to use and more reliable. You can also now directly modify the output.
> 
> Additionally, base selections are now saved between sessions. And the base list is now ordered by base size.
> 
> [Get it now on Flathub!](https://flathub.org/apps/io.github.fizzyizzy05.binary)
> ![](bBwAgKpALmxUbeivAwalCvSn.png)

[Nokse](https://matrix.to/#/@nokse22:matrix.org) says

> This week I have released a new version of Exhibit!
> 
> * Improved settings with four default configurations for different file types and the possibility to save custom ones
> * Bundled four HDRI by default, but more can be added
> * Added a button to open the file in an external app
> * Automatic reload on file change
> * More ways to color models
> * Improved point cloud support
> * Updated F3D to latest version
> 
> Get it on [Flathub](https://flathub.org/apps/io.github.nokse22.Exhibit)
> {{< video src="qjsikdaIqNzWfHfcLenYnlmc.mp4" >}}

[Alain](https://matrix.to/#/@a23_mh:matrix.org) announces

> **Discover the New Features in Planify 4.10!**
> We are excited to introduce version 4.10 of Planify, our task management app. This update brings a host of improvements and new features designed to make your life more organized and productive. Let's take a look at what's new!
> 
> **Easily Switch Between Task and Note**
> In this version, we've added a new feature that allows you to easily switch between a task and a note. Now, you can quickly change the entry type according to your needs, making it more flexible and efficient to manage your ideas and to-dos.
> 
> **Change History for Each Task**
> Have you ever wondered when you made a modification to a task? With the new change history feature, you can now see all the changes made to each task. This functionality allows you to track and revert changes, ensuring you always have full control over your tasks.
> 
> **Notifications for Tasks with Specific Times**
> Planify now sends notifications when you create a task with a specific time. This feature helps you never miss an important appointment or crucial reminder. Set your tasks with times and receive real-time alerts to stay on top of your commitments.
> 
> **Support for Integrating Multiple Todoist or Nextcloud Accounts**
> For those managing multiple accounts, we have good news! Planify 4.10 now supports integrating multiple Todoist or Nextcloud accounts. Sync your tasks and notes from different accounts in one place, simplifying the management of your information.
> 
> **New Website**
> We are also thrilled to announce that Planify has a new website! Visit us at [useplanify.com](https://useplanify.com/#home) to learn more about our app, get support, and stay updated on the latest news and features.
> 
> We hope these new features make your experience with Planify even better. We are committed to continuously improving and adding functionalities that make managing your tasks and projects easier. [Download](https://flathub.org/apps/io.github.alainm23.planify) version 4.10 today and discover all the enhancements for yourself!
> ![](UBtvOazIHxBcLYSFHCDKJtCY.png)
>
> ![](kUyGZAfClIyQCqODruStinZX.png)
>
> ![](cKeuMJJTmgsMeNyqfyLpWBqt.png)

### Fractal [↗](https://gitlab.gnome.org/World/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) says

> Because we know you can’t get enough of us, we are back again, and this time, as foretold, it’s to announce Fractal 8!
> 
> There have been little changes since our release candidate, so let’s recap the main improvements since Fractal 7:
> 
> * Mentions are sent intentionally
> * Authenticated media are supported
> * Draft messages are kept per-room and persisted across restarts
> * More links are detected in messages and room descriptions
> * Collapsed categories in the sidebar are remembered between restarts, with the “Historical” category collapsed by default
> * A banner appears when synchronization with the homeserver fails too many times in a row
> * The verification and account recovery processes have been polished
> * HTML rendering has been improved, with the support of new elements and attributes
> 
> As usual, this release includes other improvements and fixes thanks to all our contributors, and our upstream projects.
> 
> We want to address special thanks to the translators who worked on this version. We know this is a huge undertaking and have a deep appreciation for what you’ve done. If you want to help with this effort, head over to [Damned Lies](https://l10n.gnome.org/).
> 
> This version is available right now on [Flathub](https://flathub.org/apps/org.gnome.Fractal).
> 
> We have a lot of improvements in mind for our next release, but if you want a particular feature to make it, the surest way is to implement it yourself! Start by looking at our [issues](https://gitlab.gnome.org/World/fractal/-/issues/) or just come say hello in [our Matrix room](https://matrix.to/#/#fractal:gnome.org).
> ![](d10f76e06817859fa1e4de4cd72afd34df76c2151818982620424306688.png)

# Shell Extensions

[Just Perfection](https://matrix.to/#/@justperfection:gnome.org) reports

> The Extensions Port Guide for GNOME Shell 47 is Ready!
> https://gjs.guide/extensions/upgrading/gnome-shell-47.html

# Miscellaneous

[Sophie 🏳️‍🌈 🏳️‍⚧️ (she/her)](https://matrix.to/#/@sophieherold:gnome.org) says

> GNOME 47 [enters feature, UI, and API freeze](https://discourse.gnome.org/t/gnome-47-beta-newstable-tarballs-due-responsible-abderrahim-gnome-46-4-stable-tarballs-due-responsible-alatiera-gnome-45-9-oldstable-tarballs-due-responsible-jbicha-and-more/22562) the night of this Saturday (Aug 3). The release of the beta version of GNOME 47 is scheduled for next week.

# GUADEC 2024

[Caroline Henriksen](https://matrix.to/#/@chenriksen:gnome.org) announces

> GUADEC 2024 talk videos and photos are now available! You can find the individual talk videos in our [GUADEC 2024 Playlist](https://www.youtube.com/playlist?list=PLcb5uDX8FIoAzmw9QWEJMY51Yoy3_CXPs) and photos from our venue in Denver in our [shared album](https://flic.kr/s/aHBqjBC58f)
> 
> We've also prepared two post-event surveys, one for in-person attendees and one for remote. Your participation in these surveys helps us improve future GUADECs so please take a few minutes to fill it out while the event is still fresh in your mind.
> [Take the In-Person Survey](https://events.gnome.org/event/209/surveys/122)
> [Take the Remote Survey](https://events.gnome.org/event/209/surveys/125)

# GNOME Foundation

[Pablo Correa Gomez](https://matrix.to/#/@pabloyoyoista:matrix.org) reports

> The GNOME Foundation Board of Directors is now fully onboarded, and has started working following GUADEC. We have provided a small update on the work done so far: https://discourse.gnome.org/t/update-from-the-new-board/22568 and we hope to continue providing regular updates in the following months!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
