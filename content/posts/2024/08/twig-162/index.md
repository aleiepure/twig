---
title: "#162 Late Friday Edition"
author: Felix
date: 2024-08-23
tags: ["flatsync"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from August 16 to August 23.<!--more-->

# GNOME Circle Apps and Libraries

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) announces

> This week [Resources](https://flathub.org/apps/net.nokyan.Resources) was accepted into GNOME Circle. Resources lets you keep an extra eye on system resources with style. Congratulations!
> ![](0fdbcd1d6a14f9fa88d904ee69aba97b9094280d1827008454573686784.png)

# Third Party Projects

[xjuan](https://matrix.to/#/@xjuan:gnome.org) says

> I am pleased to announce a new development release of Cambalache,
> getting us one step closer to a stable release for GNOME 47.
> 
> Whats new in version 0.91.3:
>  - Support 3rd party libraries
>  - Improved Drag&Drop support
>  - Streamline headerbar
>  - Ported treeview to column view
> 
> Read more about it at https://blogs.gnome.org/xjuan/2024/08/23/new-cambalache-development-release
> ![](67ad567d773a24a1e0e3f86b7861ee50165f57431827085480450064384.png)

# Internships

### FlatSync [↗](https://gitlab.gnome.org/Cogitri/flatsync)

Keep your Flatpak apps synchronized between devices

[IlChitarrista](https://matrix.to/#/@ilchitarrista:matrix.org) announces

> FlatSync's Ignored Apps implementation is now [merged](https://gitlab.gnome.org/Cogitri/flatsync/-/merge_requests/64)!
> It will be possible to make FlatSync ignore certain apps and automatically installed dependencies are excluded by default.
> Adwaita was also updated to 1.6 with the new Button Row and Spinner.
> 
> As Google Summer of Code comes to an end, having achieved the planned features, we're now looking into further improvements and preparations for Flathub release and GNOME Circle acceptance.
> I'm very grateful for this Internship, it has been a beautiful way to start contributing; I want to thank everyone involved, especially my mentor [Cogitri](https://gitlab.gnome.org/Cogitri).
> 
> Keep tuned for further work!
> {{< video src="qlKzhhcXPIVKvuyQIyylQeSN.mp4" >}}

# Miscellaneous

[Regina](https://matrix.to/#/@reginadata:gnome.org) announces

> Sharing an Independent study on “Assessing the Effectiveness of GNOME’s Diversity and Inclusion Strategies: Challenges, Opportunities, and Recommendations.” This article explores GNOME's efforts in promoting diversity and provides actionable recommendations for improvement. Thank you to Robert ramcq , Federico federico , and Michael Downey for their reviews of this work. Your feedback is greatly valued.
> Details here: https://www.linkedin.com/pulse/assessing-effectiveness-gnomes-diversity-inclusion-regina-nkenchor-0iokf/

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
