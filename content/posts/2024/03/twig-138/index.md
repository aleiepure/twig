---
title: "#138 Refreshing Changes"
author: Felix
date: 2024-03-08
tags: ["apostrophe", "warp", "gjs", "phosh", "just-perfection"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from March 01 to March 08.<!--more-->

# Sovereign Tech Fund

[Sonny](https://matrix.to/#/@sonny:gnome.org) reports

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) project, a number of community members are working on infrastructure related projects.
> 
> Here are the highlights of the last 2 weeks.
> 
> ### Accessibility
> 
> Dorotha joined the team to work on global shortcuts portal for GNOME and better screen reader support on Wayland.
> 
> Andy landed Spiel support in Orca [#182](https://gitlab.gnome.org/GNOME/orca/-/merge_requests/182)
> 
> [Spiel](https://github.com/eeejay/spiel) is a speech synthesis (TTS) API and framework
> 
> [Orca](https://gitlab.gnome.org/GNOME/orca) is the screen reader of the Linux desktop
> 
> ### Hardware Support
> 
> Ivan [published experiments](https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/7375#note_2026507) on the extra frame of latency in GNOME Shell
> 
> Ivan [benchmarked and measured latency](https://gitlab.gnome.org/-/snippets/6439) of VTE (with Ptyxis), comparing together GTK's three renderers (current GL, new GL, new Vulkan)
>
> ![](ba590a55e5a1c8e855efd935bb89b6b2e7f878d31766171132358033408.png)
> 
> Jonas landed [h264 (software) encoding for screencasts](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3211) -  recordings in GNOME 46 will be smoother on slow hardware and have better compatibility on the web
> 
> Dor landed [variable refresh rate support](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/1154)
> 
> ### Platform
> 
> Tobias submitted a [mockup for extending background apps](https://gitlab.gnome.org/Teams/Design/os-mockups/-/blob/master/background-apps/background-apps-redux.png) with
> 
> * Dynamic actions that can change at runtime
> * Show background apps in the dash with a dimmed dot indicator
> * Show the status string and actions in the dash menu
>
> ![](49f4d6ef6e2147ad0970c9a8e17eac8eb496a4991766170917106352128.png)
>
> Julian [opened a draft for notifications API/portal v2](https://github.com/flatpak/xdg-desktop-portal/pull/1298)
> 
> Julian [landed expandable notifications in calendar drawer](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3173) - coming to GNOME 46
>
> {{< video src="4a223e4585ef561bce4efdf25222df140109619b1766170780367847424.webm" >}}
> 
> Julian solved unecessary cases of `<Application> is ready` notifications [1](https://github.com/flatpak/xdg-desktop-portal-gtk/pull/468), [2](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3199), [3](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3198), [4](https://gitlab.gnome.org/GNOME/libnotify/-/merge_requests/38)
> 
> ### Flatpak
> 
> Hub [landed supports for the settings portal in libportal](https://github.com/flatpak/libportal/pull/143)
> 
> Hub [opened a draft to implement fallback devices in Flatpak](https://github.com/flatpak/flatpak/pull/5708)
> 
> Georges [submitted a patch to support geolocation in sandboxed Flatpak WebKit applications](https://github.com/WebKit/WebKit/pull/25496)
> 
> Georges [submitted a patch to support Drag'n Drop in Flatpak WebKit applications](https://github.com/WebKit/WebKit/pull/25575)
> 
> ### Home Encryption
> 
> Adrian landed [Freeze user sessions for all types of sleep](https://github.com/systemd/systemd/pull/30612) in systemd
> 
> Adrian landed [session fields for user-record](https://github.com/systemd/systemd/pull/31310) in systemd

# GNOME Core Apps and Libraries

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) says

> This week GJS 1.79.90 was released, the release candidate for GNOME 46. In this release we have a crash fix and some preparations for performance improvements.
> 
> Also, have you ever tried using `WeakRef` or `FinalizationRegistry` in GJS and noticed ... that they don't actually work? Due to not realizing that we had to do something on our end to enable these when Mozilla added them to the JS engine, it turns out the `WeakRef` would actually create a strong ref, and the `FinalizationRegistry`'s callbacks would never be called. This is fixed now and you can use them with confidence because the functionality is now covered by tests! We also wrote [documentation for Mozilla's JS embedders repo](https://github.com/mozilla-spidermonkey/spidermonkey-embedding-examples/pull/75) to prevent problems like this in the future.

# GNOME Circle Apps and Libraries

[Hugo Olabera](https://matrix.to/#/@hugolabe:matrix.org) announces

> I just published version 3 of Wike, which is renewed to adapt to the new design styles in GNOME applications. It also adds some new features and the usual bunch of improvements and fixes.
> 
> * Side panel redesign that is now located on the left.
> * New left bar that provides quick access to all side panel elements.
> * Search moves to side panel.
> * New design for the languages selection window.
> * Selection mode added to the bookmarks and history lists.
> * New option to hide the tab bar in desktop mode.
> * New option to show all languages in language links.
> * Added match counter for text searches.
> * Switching to libsoup for Wikipedia queries.
> * New and updated translations.
> 
> Thanks to all contributors and translators!
> ![](KRANnLzunbZHqHewxYRArGeq.png)
> ![](WHFerGDygjOwNZhTUXzoEmnC.png)

### Warp [↗](https://gitlab.gnome.org/World/warp)

Fast and secure file transfer.

[Fina](https://matrix.to/#/@felinira:matrix.org) announces

> Warp 0.7 beta 1 was released to Flathub beta. It includes experimental support for QR code scanning via PipeWire and the camera portal. This feature allows to initiate a file transfer just by scanning a code on the receiving device. Any feedback is much appreciated. 📸️
> 
> To install the beta, follow the instructions in the beta announcement.
> ![](KouiCMiUHqsukZcGEISImRgW.png)

### Apostrophe [↗](https://gitlab.gnome.org/World/apostrophe)

A distraction free Markdown editor.

[Manu](https://matrix.to/#/@somas95:gnome.org) says

> Alice has done a bunch of work on Apostrophe to port widgets to their new libadwaita counterparts, as well as improving the overall styling
> ![](133e777f03b9be94197bcdeb9ecc442f8a6b6c551766199721564569600.png)
> ![](19f2aa62b699708bbc8bc65d6de8a7cdfaca2c811766199560171945984.png)

# Third Party Projects

[slomo](https://matrix.to/#/@slomo:matrix.org) says

> The GStreamer team is thrilled to announce a new major feature release of your favourite cross-platform multimedia framework!
> 
> The 1.24 release series adds new features on top of the 1.22 series and is part of the API and ABI-stable 1.x release series.
> 
> As always, this release is again packed with new features, bug fixes and many other improvements.
> 
> # Highlights
> 
> * New Discourse forum and Matrix chat space
> * New Analytics and Machine Learning abstractions and elements
> * Playbin3 and decodebin3 are now stable and the default in gst-play-1.0, GstPlay/GstPlayer
> * The va plugin is now preferred over gst-vaapi and has higher ranks
> * GstMeta serialization/deserialization and other GstMeta improvements
> * New GstMeta for SMPTE ST-291M HANC/VANC Ancillary Data
> * New unixfd plugin for efficient 1:N inter-process communication on Linux
> * cudaipc source and sink for zero-copy CUDA memory sharing between processes
> * New intersink and intersrc elements for 1:N pipeline decoupling within the same process
> * Qt5 + Qt6 QML integration improvements including qml6glsrc, qml6glmixer, qml6gloverlay, and qml6d3d11sink elements
> * DRM Modifier Support for dmabufs on Linux
> * OpenGL, Vulkan and CUDA integration enhancements
> * Vulkan H.264 and H.265 video decoders
> * RTP stack improvements including new RFC7273 modes and more correct header extension handling in depayloaders
> * WebRTC improvements such as support for ICE consent freshness, and a new webrtcsrc element to complement webrtcsink
> * WebRTC signallers and webrtcsink implementations for LiveKit and AWS Kinesis Video Streams
> * WHIP server source and client sink, and a WHEP source
> * Precision Time Protocol (PTP) clock support for Windows and other additions
> * Low-Latency HLS (LL-HLS) support and many other HLS and DASH enhancements
> * New W3C Media Source Extensions library
> * Countless closed caption handling improvements including new cea608mux and cea608tocea708 elements
> * Translation support for awstranscriber
> * Bayer 10/12/14/16-bit depth support
> * MPEG-TS support for asynchronous KLV demuxing and segment seeking, plus various new muxer features
> * Capture source and sink for AJA capture and playout cards
> * SVT-AV1 and VA-API AV1 encoders, stateless AV1 video decoder
> * New uvcsink element for exporting streams as UVC camera
> * DirectWrite text rendering plugin for windows
> * Direct3D12-based video decoding, conversion, composition, and rendering
> * AMD Advanced Media Framework AV1 + H.265 video encoders with 10-bit and HDR support
> * AVX/AVX2 support and NEON support on macOS on Apple ARM64 CPUs via new liborc
> * GStreamer C# bindings have been updated
> * Rust bindings improvements and many new and improved Rust plugins
> * Rust plugins now shipped in packages for all major platforms including Android and iOS
> * Lots of new plugins, features, performance improvements and bug fixes
> 
> Full release notes can be found at: https://gstreamer.freedesktop.org/releases/1.24/

[Can Lehmann](https://matrix.to/#/@josh-leh:matrix.org) announces

> This week, [Owlkettle 3.0.0](https://github.com/can-lehmann/owlkettle) has been released! Owlkettle is a declarative GUI framework for the Nim programming language based on GTK 4. This release wraps 27 new widgets and improves on the documentation:
> 
> * 27 new GTK & libadwaita widgets
> * Support for custom CSS classes & inline stylesheets
> * Generate interactive widget examples with the `owlkettle/playground` module
> * `private` and `onlyState` modifiers
> * Documentation website with guides on installation, application architecture and wrapping new widgets
> 
> See the full [changelog here](https://github.com/can-lehmann/owlkettle/releases/tag/v3.0.0). This is a major release which contains breaking changes. A migration guide can be found [here](https://can-lehmann.github.io/owlkettle/docs/migrating_2_to_3.html).
> 
> Thanks to all contributors!

[rdbende](https://matrix.to/#/@rdbende:matrix.org) reports

> After almost two years, we released [Cozy 1.3](https://flathub.org/apps/com.github.geigi.cozy) last week! This release brings an updated user interface along with numerous bug fixes and improved performance.
> 
> The user interface has been ported to GTK4 and Libadwaita. Thus, Cozy benefits from the new style sheet, automatic dark mode, and utilizes the latest and greatest UI elements throughout the application.
> 
> Other changes include:
>   - Improved mobile support
>   - Smaller visual refinements to match the state of the art of GNOME apps
>   - Dozens of bug fixes and performance improvements
>   - Significant cleanup and improvements to the codebase
>   - As always, updated translations thanks to all translators!
>   
> Many thanks to Julian Geywitz for the great app and codebase, and to all the contributors who helped make this release happen!
> ![](NLhZFkuQmlVvYNWTCbqMTsNF.png)
> ![](VqJHYZnjpvBYaAsfhbdlKWkP.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) says

> [Phosh](https://gitlab.gnome.org/World/Phosh) 0.37.0 is out:
> 
> * Wi-Fi networks can now be selected from quick settings
> * Add your own Custom quick settings via plugins
> * There's a new caffeine quick setting using that
> * Support cutouts and notches of 16 more phones
> 
> There's more. Check the full details [here](https://phosh.mobi/releases/rel-0.37.0/)
> ![](vxHNQhmMGKlgWmRLnimrLnft.png)

# Shell Extensions

### Just Perfection [↗](https://extensions.gnome.org/extension/3843/just-perfection/)

A tweak tool to customize the GNOME Shell and to disable UI elements.

[Just Perfection](https://matrix.to/#/@justperfection:gnome.org) reports

> [Just Perfection extension](https://extensions.gnome.org/extension/3843/just-perfection/) is ported to GNOME Shell 46.
> We have a new feature in this version called _window maximized by default_ to open all windows in maximized automatically.
> This version is named after English artist Edward Lear.
> https://www.youtube.com/watch?v=FBMM8s2J2zI

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
