---
title: "#132 Bottom Sheets"
author: Felix
date: 2024-01-26
tags: ["gtk", "gnome-maps", "fragments", "libadwaita"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 19 to January 26.<!--more-->

# Sovereign Tech Fund

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) project, a number of community members are working on infrastructure related projects.
> 
> Today we celebrate [Sophie](https://blogs.gnome.org/sophieh/) joining the team to work on [Glycin](https://gitlab.gnome.org/sophie-h/glycin) to work on
> * Improved sandboxing for image loaders
> * GObject Introspection support to broaden interoperability with the GNOME platform
> 
> Accessibility
> 
> * Joanie added a system information presenter in Orca
> * Joanie finished code clean-up and removal of pyatspi dependency for hypertext and hyperlink interfaces
> * Joanie started code clean-up creation of AT-SPI2 utilities for Orca's accessible-text related functionality [issue](https://gitlab.gnome.org/GNOME/orca/-/issues/300)
> * Joanie [made a proposal](https://gitlab.gnome.org/GNOME/at-spi2-core/-/issues/154) to facilitate text selection via ATK/AT-SPI2 across multiple objects at once (similar to what IAccessible2 created)
> * Joanie [made a proposal](https://gitlab.gnome.org/GNOME/at-spi2-core/-/issues/153) to have an attributes-changed signal for object attributes
> * Joanie began converting Orca's WebKitGtk support over to the generic web support currently shared by Chromium and Gecko.
> * Matt pushed a partial [Wayland protocol extension](https://gitlab.freedesktop.org/mwcampbell/wayland-protocols/tree/accessibility) for accessibility consumers (screen readers and the like) 
> * Matt started implementation the accessibility extension as a [proof of concept in Mutter](https://gitlab.gnome.org/mwcampbell/mutter/tree/wayland-native-a11y)
> * Tobias investigated where we still use TreeViews and [started an initiative](https://gitlab.gnome.org/GNOME/Initiatives/-/issues/49) to port to more accessible widgets (e.g. ListView, ColumnView) 
> * Evan landed [gtk: Add AccessibleList to enable relations in bindings](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/6807)
>   - enables languages like GJS and Python to pass lists of Gtk widgets to accessibility relations like LABELLED_BY in GTK4
>   - started a GJS MR for it to apply a convenience override to automatically wrap JS arrays in Gtk.AccessibleList in the relevant APIs
> * Georges is working on WebKitGTK accessibility
>   - experimented with a potential new GTK4 API to be consumed by WebKitGTK. The experiment was a success and it correctly bridged the web page DOM a11y tree with the rest of the program, which allows screen readers and other accessible technologies to read that. I'm currently cleaning up the code and discussing the approach with GTK developers.
>   - published and improved [Aleveny](https://gitlab.gnome.org/feaneron/aleveny) a tool to inspect the accessible object tree of apps.
> * Sonny helped with coordination efforts to land [High Contrast hint on settings portal](https://github.com/flatpak/xdg-desktop-portal/pull/1175)
>   - Hub will work on [GNOME backend implementation](https://gitlab.gnome.org/GNOME/xdg-desktop-portal-gnome/-/issues/119)
>
> ![](f55405e23cea5dde117a68ab1046e15a26ab85df1750972228364664832.png)
> 
> Platform
> 
> * Hub fixed a [bug in flatpak-builder rename-appdata-file](https://github.com/flatpak/flatpak-builder/pull/579)
> * Julian landed [using libadwaita Avatar in gnome-initial-setup](https://gitlab.gnome.org/GNOME/gnome-initial-setup/-/merge_requests/215)
> * Julian is working on extending the XDG portal notification API with sounds and images [portal issue](https://github.com/flatpak/xdg-desktop-portal/issues/983)
> * Sonny started a [proof of concept for a GTK linter](https://gitlab.gnome.org/jwestman/blueprint-compiler/-/merge_requests/176)
> * Stef joined the team and started working on [GFileMonitor does not work with document portal](https://github.com/flatpak/xdg-desktop-portal/issues/567)
> * Evan made progress on async/sync annotations support in introspection, there are currently 4 MRs for it
>   - https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3797
>   - https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3853
>   - https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3830
>   - https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3746
> * Evan is investigating the amount of work needed to make WASM support in GJS production ready - currently evaluating the mergeability of multi-threaded promises and import maps
> * Philip fixed the following in GLib
>   - [Segfault in gi_function_info_prep_invoker](https://gitlab.gnome.org/GNOME/glib/-/issues/3218)
>   - [gibaseinfo: Fix use of stack-allocated GIBaseInfos](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3846)
>   - [girepository: Drop gi_repository_get_default()](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3856)
>   - [Reference cycle between GIRepository and GIBaseInfos cached by it](https://gitlab.gnome.org/GNOME/glib/-/issues/3234)
>   - [girnode: Document ownership and element types of internal structs](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3854)
> * Philip released GLib 2.78.4 and 2.79.1
> * Alice finished and landed [adaptive dialogs](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1018) (see her individual update below)
> 
> Hardware support
> 
> * Dor continued iterating on [VRR configuration UX in Settings](https://gitlab.gnome.org/GNOME/gnome-control-center/-/issues/2523#note_1971815)
> * Dor investigated and fixed a number of issues related to VRR
>   - [Remove assumption about fixed refresh rate in empty frame handling](https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3521)
>   - [Cursor movement becomes synchronized with main content updates after VT switch](https://gitlab.gnome.org/GNOME/mutter/-/issues/3259)
> 
> ![](ec4582d585a6c84eb3c561dc55753708a7771a6f1750973560861491200.png)
> * Alice landed bottom sheets in libadwaita https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1018
>   - Documentation https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.Dialog.html
>   - There's also a migration guide for porting apps to using it https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/migrating-to-adaptive-dialogs.html
> * Jonas (Dreßler) is investigating remaining issues in Jonas (Ådahl) fractional scaling branch
> 
> Security:
> 
> * Dhanuka continued his work on implementing secret server/backend in oo7 https://github.com/bilelmoussaoui/oo7/pull/56
>   - implemented `CreateCollection` and `SearchItems` on `org.freedesOnceCellktop.Secret.Service` interface
>   - implemented `Delete` on `org.freedesktop.Secret.Item` interface
>   - updated `CreateItem` on `org.freedesktop.Secret.Collection` to use `oo7::dbus::api::properties::Properties`
> * We are investigating and coordinating usage of [systemd per-user encrypted credentials](https://github.com/systemd/systemd/pull/30968)

# GNOME Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alice (she/her)](https://matrix.to/#/@alexm:gnome.org) says

> [`AdwDialog`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.Dialog.html) has landed, along with [`AdwAlertDialog`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.AlertDialog.html), [`AdwPreferencesDialog`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.PreferencesDialog.html) and [`AdwAboutDialog`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.AboutDialog.html). There's also a [migration guide](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/migrating-to-adaptive-dialogs.html) for all of the new widgets. The old widgets aren't deprecated yet, but will be in GNOME 47
> {{< video src="f0f069452220a38eb6a22b2255adc0c9baf8e04c1750946597975359488.mp4" >}}

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Matthias Clasen](https://matrix.to/#/@matthiasc:gnome.org) says

> The GTK 4.13.6 release out this week changes the default renderer to be the ngl renderer.
> 
> The intent of this change is to get wider testing and verify that the new renderers are production-ready. If significant problems show up, it may get reverted for the stable 4.14 release in March.
> 
> You can still override the renderer choice using the GSK_RENDERER environment variable.
> 
> Since ngl can handle fractional scaling much better than the old gl renderer, fractional scaling is now enabled by default with gl.
> 
> If you are using the old gl renderer (e.g. because your system is limited to GLES2), you can disable fractional scaling by setting the GDK_DEBUG environment variable to include the gl-no-fractional key.

### Maps [↗](https://wiki.gnome.org/Apps/Maps)

Maps gives you quick access to maps all across the world.

[mlundblad](https://matrix.to/#/@mlundblad:matrix.org) says

> Maps now shows an empty state for the favorites menu, and also allows removing favorites directly from the popover (with an undo toast). Also James Westman has improved GeoJSON shapelayer rendering, show descriptions for marked places, and also shows the layer name in the bubbles
> ![](gdnQzesBfJhGqrRSXAbFdiEF.png)
> ![](BlwfuKfoppWrnNNwpSObCTry.png)
> ![](IdeYYADZnirrdbtPDNbQEPMS.png)
> ![](ykvbMLazOFGiUuVwWDydbfWh.png)

# GNOME Circle Apps and Libraries

### Fragments [↗](https://gitlab.gnome.org/World/Fragments)

Easy to use BitTorrent client.

[Felix](https://matrix.to/#/@felix:haecker.io) announces

> Fragments now allows you to search for added torrents 🔎
> {{< video src="znRKVwnQYCTGXLWddfLoNmRr.webm" >}}

# Third Party Projects

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> [oo7](https://github.com/bilelmoussaoui/oo7), a Rust client library for interacting with the system keyring, received two new additions:
> 
> * A rewrite of secret-tool, a cli application to interact with the keyring
> * A rewrite of the secret portal
> 
> On top of that Dhanuka Warusadura have been working on a server side implementation
> ![](2e5bc670ab0fae61af708597d53fb5a4c9fdaec41750614100586528768.png)

[badcel](https://matrix.to/#/@badcel:matrix.org) announces

> I published the repository [Maus](https://github.com/badcel/Maus) containing an early stages Adwaita C# app which allows to configure a Microsoft Intellimouse Pro. Feedback is welcome.

# Miscellaneous

[Cassidy James](https://matrix.to/#/@cassidyjames:gnome.org) reports

> Flathub, the app store developed by KDE, GNOME, and independent contributors, has [announced over one million active users](https://docs.flathub.org/blog/over-one-million-active-users-and-growing)! This means when you bring your app to Flathub—either independently or as a part of [GNOME Circle](https://circle.gnome.org/)—you're reaching a potential audience of over a million Linux users.

[Dorothy K](https://matrix.to/#/@dorothyk:matrix.org) reports

> As Outreachy Interns,for the past couple of weeks Tanjuate and I have been working on implementing end-to-end testing for GNOME with openQA for [Outreachy](https://www.outreachy.org/) and our focus in the last few weeks has been a11y tests for GNOME OS.We have written tests for accessibility features ie, High contrast,Large text,Overlay scrollbars, Screen reader, Zoom, Over amplification,Visual alerts and On Screen Keyboard features.
>
> Take a look at some of the tests we have added with a prefix “a11y-” [here](https://openqa.gnome.org/tests/3058) and this [post](https://discourse.gnome.org/t/implementing-end-to-end-tests-for-gnome-os-using-openqa-accesibility-tests-feedback/19124) for more context
> ![](outreachy.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

