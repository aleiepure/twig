---
title: "#131 STF Happenings"
author: Felix
date: 2024-01-19
tags: ["graphs", "boatswain", "workbench", "apostrophe", "gjs", "gir.core", "fractal"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 12 to January 19.<!--more-->

# Sovereign Tech Fund

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> As part of the [GNOME STF (Sovereign Tech Fund)](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) project, a number of community members are working on infrastructure related projects.
> 
> * Alice continued work on bottom sheets https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/1018
>     - implemented proper focus for bottom sheets
>     - fixed docs for ported dialogs
>     - implemented swipe to close for bottom sheet dialogs
>     - split out the dialog support code from windows; experimented with putting it into tab pages: https://mk.nyaa.place/notes/9omtb0ugc3i50oiz
> {{< video src="ed1cd373808152fd9cb9fa736439ec607b3dd5e41748407311157690368.mp4" >}}
> * Georges started working on a11y support for WebkitGTK (GTK4)
> * Philip did various maintenance tasks on GLib
>     - Many reviews in GLib
>     - More libgirepository cleanups, https://gitlab.gnome.org/GNOME/glib/-/issues/3155
>     - More libgirepository cleanups, https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3836
>     - Writing unit tests for libgirepository, https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3836
>     - Validating GLib’s tests, https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3837 and https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3838
>     - Reviewing gi-docgen docs linking changes, https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3809
> * Evan continued working on GObject Introspection
>     - Unblock adding `g-ir-compiler` into the GLib repository
>         - https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3830
>         - https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3797
>     - Working on support for compiling `g-ir-compiler2`/`gir-compiler` from GLib
>     - Async support for GLib in typelibs https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3746
> * Dhanuka continued his work on implementing secret server/backend in oo7 https://github.com/bilelmoussaoui/oo7/pull/56
>     - Completed org.freedesktop.Secret.Collection interface implementation
>     - Completed org.freedesktop.Secret.Session interface implementation
> * Tait
>     - Got started looking into GTK+Mutter menu disappearing issue: https://gitlab.gnome.org/GNOME/gtk/-/issues/6277
>     - Discussed accessibility key binding priority https://gitlab.gnome.org/GNOME/at-spi2-core/-/issues/147
> * Jonas worked on improving app grid scaling on smaller laptop displays
> * Julian continued working on notifications
>     - Created a separate MR for adding the new header to notifications in gnome shell https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3103
>     - Rebased other outstanding MRs
>     - Started working on a prototype for notification threading support in GNOME Shell and the rest of the stack
> * Julian continued working on using Libadwaita default avatars for new users
>     - Got the avatar changes in gnome-control-center merged, required a rebase https://gitlab.gnome.org/GNOME/gnome-control-center/-/merge_requests/1939
>     - Fixed some followup issues from the review in Initial Setup https://gitlab.gnome.org/GNOME/gnome-initial-setup/-/merge_requests/215
> * Matt continued working on support for the new Wayland accessibility protocol extension in Mutter and designing the protocol for assistive technologies and other clients
> * Hub
>     - Draft PR of the internal doc for the document portal https://github.com/flatpak/xdg-desktop-portal/pull/1265
> * Dor
>     - Prototyped an alternative solution for VRR configuration in Settings following design feedback. https://gitlab.gnome.org/GNOME/gnome-control-center/-/issues/2523#note_1971322
>     - Rebased VRR MR locally on top of GNOME 46.alpha
>     - Investigated a Firefox (Wayland) frame pacing issue, preparatory MR https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/3521
>     - Started looking into a plan to get VRR merged in GNOME 46 in experimental form with reduced feature set https://gitlab.gnome.org/GNOME/mutter/-/issues/3125
> * Sam
>     - Settings:
>         - Design input on the ongoing work to implement VRR support in Displays panel: https://gitlab.gnome.org/GNOME/gnome-control-center/-/issues/2523
>         - New designs for how Text-to-Speech and speech providers can live in Settings: https://gitlab.gnome.org/Teams/Design/settings-mockups/-/issues/65
> ![](02e2528e04c45e845d0e6ab230fc59b5c9b5fffe1748407319298834432.png)
>     - GNOME Shell:
>         - fix regression in lockscreen focus style: https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3118
>         - fixes for newly introduced problems with the Dash on smaller resolutions https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3119
>         - clean up, and small fixes for the calendar popover: https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3120
>         - investigated why Alt-Tab switcher lost text to speech support in 46.alpha: https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/7336
>     - Various improvements to Adwaita Icons/Cursors
> * Adrian continued worked on blob dirs in systemd-homed https://github.com/systemd/systemd/pull/30840

[nirbheek](https://matrix.to/#/@nirbheek:matrix.org) reports

> Last week, the foundation of a full Rust rewrite of [GStreamer's](https://gstreamer.freedesktop.org) Real-Time Protocol (RTP/RTCP) network stack was created, with the effort funded by the Sovereign Tech Fund. You can read
> [STF's announcement on Mastodon](https://mastodon.social/@sovtechfund/111771701228506471).
> 
> RTP, RTCP, and SDP are standard specifications which constitute the basic building blocks on top of which widely-used real-time protocols like RTSP (Real-Time Session Protocol) and WebRTC (Web Real-Time Communication) have been created. You can read more about this and the project in general at https://www.sovereigntechfund.de/tech/gstreamer
> 
> The rewrite has several technical advantages over the existing C-based stack
> built ontop of GObject and GIO:
> 
> 1. Improved memory safety: important when parsing untrusted network data
> 2. More flexible architecture: the existing RTP stack was "layer cake" which made adding new interactions and features hacky and cumbersome
> 3. Better performance: the previous architecture suffered from lock contention, excessive use of threads, and the overhead of repeatedly mapping and unmapping buffers due to the "layer cake" architecture
> 4. Easier for newcomers: the aforementioned "layer cake" of abstractions made it difficult for non-experts to understand the stack, and we expect new contributors to have an easier time helping, which helps with sustainability
> 5. Lower cognitive overhead: the revised architecture and the use of async Rust will reduce the effort required to write new features, refactor code, and generally increase maintainability
> 
> The new stack is in a functional and usable state, but lacks some advanced features that are needed for being a complete rewrite of the existing stack. You can follow the progress in the below merge requests:
> 
> * [New RTP manager and jitterbuffer](https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs/-/merge_requests/1426)
>   - Measured to be about 50% faster, and the flexible architecture should make it easier to write another jitterbuffer implementation with dynamic latency
> * [New RTP payloading / depayloading base classes](https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs/-/merge_requests/1424)
>   - More payloaders and depayloaders will be submitted once the base classes have undergone code review and been merged.
> * [New RTSP source element](https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs/-/merge_requests/1425)
>   - Currently only live-streaming is supported, Video-on-Demand (VOD) is planned for the future
>
> ![](BXaLLQRSyAHSlLRdVeZSLPqS.png)

# GNOME Core Apps and Libraries

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[ptomato](https://matrix.to/#/@ptomato:gnome.org) says

> We released extra versions of GJS, 1.78.3 (stable) and 1.79.2 (unstable) to fix a regression that made extension preferences windows crash in GNOME Shell. The 1.79.2 release also contains several performance improvements from Marco Trevisan!

# GNOME Circle Apps and Libraries

### Graphs [↗](https://graphs.sjoerd.se)

Plot and manipulate data

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) reports

> This week [Graphs](https://apps.gnome.org/Graphs/) was accepted into GNOME Circle. Graphs is a simple, yet powerful tool that allows you to plot and manipulate your data with ease. Congratulations!
> ![](85c3a45ad220e7cb5f0bd74f038dfe2551a716f21746350409070411776.png)

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> Workbench, the Code Playground for GNOME is out in version 45.4 🛠️
> https://flathub.org/apps/re.sonny.Workbench
> 
> 
> The highlights of this release are
> 
> 
> * Restore on-disk projects when starting Workbench
> * Open the Library on start if there are no projects to restore
> * Restore scroll and cusor positions on format and Run
> * Add "Copy" and "Select All" to Console
> * Add Vala formatter support
> * Add WebP image format support
> * Library: Add "Context Menu" demo
> * Library: Add "HTTP Server" demo
> * Library: 12 demos ported to Python, 4 to Vala and 2 to Rust
> * Use Biome instead of prettier as JavaScript formatter
> * Use GTKCssLanguageServer instead of prettier as CSS formatter
> * Fix Console style when toggling dark mode
> * Fix blank preview on demos with no code
> * Fix Style affecting other windows
> 
> Thanks to the following contributors Gregor Niehl, UrtsiSantsi, Sabri Ünal, Hofer-Julian, Roland Lötscher, Sahil Shadwal, Dan Yeaw, AkshayWarrier, Marco Köpcke, Diego Iván M.E, Sonny Piers.
> 
> Happy hacking / learning / prototyping

### Boatswain [↗](https://gitlab.gnome.org/World/boatswain/)

A guiding hand when navigating through streams.

[Georges Stavracas (feaneron)](https://matrix.to/#/@feaneron:gnome.org) says

> Due to a surprisingly popular demand, I'm preparing to add support for Elgato Stream Deck+ devices to Boatswain. For that to happen, Boatswain needs to be adjusted for new button types, a different USB protocol, and new input events. In order to test it, I'll need a device, so I started a small fundraising campaign. [You can read more here](https://feaneron.com/2024/01/15/mini-fundraiser-stream-deck-plus/).

### Apostrophe [↗](https://gitlab.gnome.org/World/apostrophe)

A distraction free Markdown editor.

[Manu](https://matrix.to/#/@somas95:gnome.org) announces

> I've been working on a new Layout Manager for Apostrophe to replace the now deprecated AdwLeaflet. This has been a nice opportunity to implement a tailor made solution for apostrophe's panels which fully adapts to the app's needs
> {{< video src="81ffadebcff4e4be369d07f5365306d573fbe1131747761683733938176.mp4" >}}

# Third Party Projects

[ghani1990](https://matrix.to/#/@ghani1990:matrix.org) says

> [Lorenzo Paderi](https://mijorus.it/) has just released [Collector](https://flathub.org/apps/it.mijorus.collector), a new application that aims to be a Dropover alternative for GNOME.
> Collector allows users to quickly and easily drag files, images, text and links onto the Collection window, and drop them anywhere!
> Collector also supports a variety of features, including:
> 
> * The ability to Open multiple Collector windows and customize their color
> * The ability to download images by Easily drag them from web browser.
> * The ability to group texts into a single, ready-to-use CSV file
> 
> If you are a user who is looking for an effortless Drag & Drop, I encourage you to check out Collector. It is a great option that is sure to meet your needs.
> ![](AeblpwqHJpIgJjRZVeJMCwYt.png)
> {{< video src="uGyCMBDbPlJonEqJfMCZoRpk.webm" >}}

[Gianni Rosato](https://matrix.to/#/@computerbustr:matrix.org) reports

> Today, we have a very exciting development - Aviator has switched to an in-house custom fork of SVT-AV1 dubbed SVT-AV1-PSY!
> 
> Featuring development efforts from BlueSwordM (author of the previous custom fork Aviator has been occaisonally using), myself (Gianni), and others, this change will enable us to have much more control over the encoder's development so that it aligns most effectively with Aviator's number one priority: visual quality.
> 
> So far, SVT-AV1-PSY's most noteworthy feature is a variance boost patch that appreciably improves intra- and inter-frame fidelity consistency across wide range of content. Visual fidelity per bit is estimated to have increased by up to 5% with this patch, which currently isn't present in mainline SVT-AV1.
> 
> Even though this patch should inevitably make it to mainline, the gains from having more direct control are already being realized as we can implement features like this flexibly at a pace we decide.
> 
> Aside from that, FFmpeg has been updated to version 6.1, the SCM toggle has been replaced with a more useful Open GOP toggle, and the README has been modified to reflect some of these more recent changes with regard to SVT-AV1-PSY.
> 
> Thank you for using Aviator, and happy encoding!
> ![](FYUNVyIODlrbzOKHahMqXNnX.webp)

### Gir.Core [↗](https://gircore.github.io/)

Gir.Core is a project which aims to provide C# bindings for different GObject based libraries.

[badcel](https://matrix.to/#/@badcel:matrix.org) announces

> Gir.Core 0.5.0-preview.4 got released. It is the next step to the upcoming 0.5.0 release and adds a lot of public API for records. See the [release notes](https://github.com/gircore/gir.core/releases/tag/0.5.0-preview.4) for further details.

### Fractal [↗](https://gitlab.gnome.org/World/fractal)

Matrix messaging app for GNOME written in Rust.

[Kévin Commaille](https://matrix.to/#/@zecakeh:tedomum.net) says

> Fractal 6 is up and ready! 
> 
> That's right, barely 2 months after Fractal 5, we feel there have been enough improvements to grant a new stable release. You have probably noticed that we have adopted a version scheme similar to GNOME and will bump the major version with each new release.
> 
> The list of goodies:
> 
> * Fractal can open Matrix URIs, it is even registered as a handler for the `matrix` scheme
> * The verification flow was rewritten, hopefully solving most verification issues
> * Room members can be kicked, banned or ignored from their profile
> * More notifications settings, global or per-room, were added
> * Times follow the format (12h or 24h) from the system settings
> * Tab auto-completion also works for mentioning public rooms, just start your query with `#`
> 
> It is available right now on [Flathub](https://flathub.org/apps/org.gnome.Fractal).
> 
> This version is fully translated into 6 languages 🙌️ and we hope to get even more 📈 for the next one! Head over to [Damned Lies](https://l10n.gnome.org/module/fractal/) if you want to give a hand.
> 
> We would also like to thank our new and returning contributors and our upstream projects.
> 
> For our next iteration, we already have a bunch of accessibility improvements waiting, and we plan to continue improving the room settings and the moderation features. Any extra work [from you 🫵️](https://gitlab.gnome.org/GNOME/fractal/-/issues/?label_name%5B%5D=4.%20Newcomers) will be highly valued!
> ![](7386389379feef250a2fd74bf892217d743b48bc1748042859015045120.png)

# Events

[Pedro Sader Azevedo](https://matrix.to/#/@toluene:matrix.org) reports

> The GNOME Africa community has been hard at work on organizing a preparatory Bootcamp for those who intend to apply for Google Summer of Code (GSoC) and Outreachy internships.
> 
> The goal for this event is to equip participants with the necessary technical skills, project-specific knowledge, mentorship guidance, and community insights, thereby increasing their competitiveness and readiness to successfully contribute to GNOME during the application cycles of these internship programs.
> 
> The event will be held entirely online, so people from anywhere in the world are welcome to join. It will happen from January 22<sup>nd</sup> to February 2<sup>nd</sup>, so save those dates!
> 
> If you're interested in attending, please register for the event in the following link: https://events.gnome.org/event/244/
> ![](jeTEzocacUrKBLfVOywxvOUl.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

