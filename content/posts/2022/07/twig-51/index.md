---
title: "#51 About New Apps"
author: Thib
date: 2022-07-08
tags: ["workbench", "libadwaita", "gnome-todo"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 01 to July 08.<!--more-->

# Core Apps and Libraries

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) reports

> Following message dialogs, Libadwaita now has [`AdwAboutWindow`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.AboutWindow.html)
> ![](6fe7138adae430ac906c096c1c5136421b4ef85b.png)

# Third Party Projects

[Paulo](https://matrix.to/#/@raggesilver:matrix.org) says

> Announcing [Black Box](https://gitlab.gnome.org/raggesilver/blackbox/) — a beautiful Gtk 4 terminal.
> 
> It features customizable tabs, a toggleable header bar, floating window controls, fullscreen support, and Tilix-compatible full-window theming. Last and most importantly, Black Box has tabs in the header bar.
> 
> Black Box is written in Vala and built on top of Gtk 4, libadwaita, and VTE. Check it out!
> ![](LbbJEdLwTTmGtfpiQLbJlYuY.png)

### Workbench [↗](https://github.com/sonnyp/Workbench/)

A sandbox to learn and prototype with GNOME technologies.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) reports

> A new version of Workbench, the app to learn and prototype with GNOME technologies, is out.  Here are the highlights:
> 
> * Add an Icon Browser to find the right icons for your prototypes
> * Add a Platform tools Library demo to learn about/from GTK Inspector, Adwaita Demo, GTK Demo and GTK Widget Factory
> * Adopt the de facto standard light/dark style switcher
> * Replace confirmation dialogs with toasts and undo
> * Preview now support updates on root objects
> * Support binding signal handlers from UI
> * Add APIs to allow using templates from Code
> * Add Center/Fill preview modes for templates
> 
> https://beta.flathub.org/apps/details/re.sonny.Workbench
> ![](YpSfHbWDVZWZSNRaQJugMyCj.png)

### GNOME To Do / Endeavour [↗](https://gitlab.gnome.org/GNOME/gnome-todo)

An intuitive and powerful application to manage your personal tasks.

[Jamie (She/Her)](https://matrix.to/#/@itsjamie9494:matrix.org) reports

> GNOME To Do has been renamed to Endeavour, with a new feature making it possible to drag tasks between lists

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
