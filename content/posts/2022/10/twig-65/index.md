---
title: "#65 Officially Deprecated"
author: Felix
date: 2022-10-14
tags: ["tubeconverter", "bottles", "tagger", "extension-manager", "gtk", "video-trimmer", "eyedropper", "girens"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 07 to October 14.<!--more-->

# Core Apps and Libraries

### GTK [↗](https://gitlab.gnome.org/GNOME/gtk)

Cross-platform widget toolkit for creating graphical user interfaces.

[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> After two years of soft-deprecation, GTK 4.10 (planned for March 2023) will officially deprecate GtkTreeView, GtkIconView, GtkComboBox, and all the API related to cell renderers. You should follow the [GTK4 migration guide](https://docs.gtk.org/gtk4/migrating-3to4.html#consider-porting-to-the-new-list-widgets) to see how to replace them with the new model objects and list view widgets.

# Circle Apps and Libraries

### Video Trimmer [↗](https://gitlab.gnome.org/YaLTeR/video-trimmer)

Trim videos quickly.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) announces

> I've released [Video Trimmer](https://flathub.org/apps/details/org.gnome.gitlab.YaLTeR.VideoTrimmer) 0.8.0! It's been updated to the GNOME 43 platform, bringing working drag-and-drop to open videos on Flatpak and a new About dialog. Video Trimmer also no longer closes when trying to open an inaccessible file.
> ![](5a8f431e107b624121b9fc695b7e8feac0e1e6d2.png)

# Third Party Projects

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> [flatpak-vscode](https://github.com/bilelmoussaoui/flatpak-vscode) 0.0.30 is out, with mostly improvements & bugfixes: 
> * Add command to show application's data directory
> * Fallback to the Flatpak-installed `flatpak-builder` (`org.flatpak.Builder`) when it is not found on host
> * Automatically resize output terminal when terminal window resizes
> * Drop rust-analyzer runnables.extraArgs target-dir override
> * Update to node v16
> * Don't require finish-args

### Tube Converter [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

An easy-to-use video downloader (yt-dlp frontend).

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> I'm proud to introduce [Tube Converter](https://beta.flathub.org/apps/details/org.nickvision.tubeconverter) . An easy-to-use video downloader (yt-dlp frontend) written in C++ with GTK4 and libadwaita! Supports running multiple downloads at once and a variety of file types (mp4, webm, mp3, opus, flac, and wav). Check it out on Flathub!
> ![](KTtijSrcwCOZMxcEizhDZONA.png)
> ![](uqXlDhnXwmDmPhTwwdLFRAin.png)

### Tagger [↗](https://flathub.org/apps/details/org.nickvision.tagger)

An easy-to-use music tag (metadata) editor.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> [Tagger](https://beta.flathub.org/apps/details/org.nickvision.tagger) is now at V2022.10.3 and has seen many new features, ux improvements, and bug fixes throughout the week.
> Here is a full changelog since last week's post:
> * Tagger will now notify the user of changes waiting to be applied to a file (A confirmation dialog will also be displayed when reloading a music folder or closing the application with changes waiting)
> * Added the ability to submit tag metadata to AcoustId
> * Added the ability to right-click the music files list when files are selected to access a tag actions context menu 
> * The 'Delete Tag' action must now be applied to be saved to the file
> * The 'Tag to Filename' action must now be applied to change the filename on disk
> * Improved 'Download MusicBrainz Metadata' accuracy and performance
> * Improved file size calculation
> * Fixed an issue where the 'Apply' action would clear the file selection
> * Fixed window sizing issues for low-res screens
> ![](FxLYYnJnHRDgBfpSsZxrokZj.png)

### Girens for Plex [↗](https://flathub.org/apps/details/nl.g4d.Girens)

Girens is a Plex Gtk client for playing movies, TV shows and music from your Plex library.

[tijder](https://matrix.to/#/@tijder:matrix.org) says

> This week I started porting [Girens](https://gitlab.gnome.org/tijder/girens/-/merge_requests/4) to Gtk4 and Libadwaita. Thanks to porting the app it got a few nice extra features:
> 
> * Wayland support for video playback.
> * Improved lists for large libraries (thanks to the new Gtk4 lists).
> * The borderless view modus is much better.
> 
> Before I can release this version, their is still a lot of work to be done.
> ![](HrzMgMydqxZKvyLwQhgUAwtp.png)

### Eyedropper [↗](https://flathub.org/apps/details/com.github.finefindus.eyedropper)

A powerful color picker and formatter.

[FineFindus](https://matrix.to/#/@finefindus:matrix.org) says

> Eyedropper now supports HWB and CIELCh formats, as well as showing the 'name' of the color specified by the CSS color keywords or the xkcd color survey.
> ![](CkLdpPkMseTQfSNJpkmjciaO.png)

### Extension Manager [↗](https://github.com/mjakeman/extension-manager)

Browse and install GNOME Shell extensions.

[firox263](https://matrix.to/#/@firox263:matrix.org) says

> Extension Manager 0.4 has been released, coinciding with 250,000+ downloads on Flathub 🎉.
> 
> It features:
>  - Fully adaptive mobile-friendly user interface
>  - Upgrade Assistant to check extension compatibility before upgrading
>  - Paginated search results
>  - Unsupported extensions are hidden by default
>  - Fullscreen image viewer
>  - Handles new `gnome-extensions://` URI scheme
> ![](vCscSTeKnoAcFkOBvkiKbCyz.png)
> ![](pEwyoAImjoluaNXtamqVllDu.png)

### Bottles [↗](https://usebottles.com/)

Easily run Windows software on Linux with Bottles!

[Hari Rana (TheEvilSkeleton)](https://matrix.to/#/@theevilskeleton:fedora.im) says

> Bottles 2022.10.14 was released!
> 
> We improved the interface by using common names to emphasize the goal of options. Previously, we used project names, but we figured that these names were unclear for newcomers, as names did not convey their intention. For example, "DXVK" was renamed to "Direct3D 9/10/11 Compatibility".
> 
> For more information about the new update, check out our [release page](https://usebottles.com/blog/release-2022.10.14/)!
> ![](b4788d934a8f13e62014c2fb99a659fc25c181f3.png)

# Miscellaneous

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) says

> The experimental GNOME OS OpenQA tests now verify that each core app starts up as expected. Here's an example test run: https://openqa.gnome.org/tests/338#

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

