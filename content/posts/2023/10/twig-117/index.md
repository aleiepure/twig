---
title: "#117 Queued Fixes"
author: Felix
date: 2023-10-13
tags: ["fretboard", "wildcard", "video-trimmer", "gir.core", "gnome-software", "cartridges"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from October 06 to October 13.<!--more-->

# GNOME Core Apps and Libraries

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) reports

> gnome-software has branched for 46, and Milan Crha has started landing various nice UI fixes which had been queued up after 45

# GNOME Circle Apps and Libraries

### Video Trimmer [↗](https://gitlab.gnome.org/YaLTeR/video-trimmer)

Trim videos quickly.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) says

> I have released Video Trimmer 0.8.2, a minor update to refresh the visuals for GNOME 45.
> ![](146e8d2cb0843af5e1e40c83b6702029e2d503f31710962576046686208.png)

### Cartridges [↗](https://github.com/kra-mo/cartridges)

Launch all your games

[kramo](https://matrix.to/#/@kramo:matrix.org) announces

> I released Cartridges 2.6 with a much requested new feature:
> 
> You can now search your game library from your desktop!
> 
> To enable the feature, toggle "Cartridges" on from the Search page in the Settings app.
> 
> You can get the app from [Flathub](https://flathub.org/apps/hu.kramo.Cartridges).
> 
> PS: If you want to support my work, I now accept donations through [GitHub Sponsors](https://github.com/sponsors/kra-mo)! 💜
> ![](KHDVSJXJoZuDFclFKoLLAMUo.gif)

# Third Party Projects

[Philipp](https://matrix.to/#/@philippun:matrix.org) says

> Migrating Meld
> 
> This is a little status update on my current progress of the [meld migration to Gtk4 / libadwaita](https://gitlab.gnome.org/GNOME/meld/-/merge_requests/105):
> 
> * MainWindow uses Adw.ApplicationWindow
> * NewDiff page and both the FileDiff and DirDiff page are also migrated (mostly)
> * The Preferences dialog uses Adw.PreferencesDialog
> * Most of the signals have been migrated to event controllers
> * Most methods have been migrated to new signature (i.e. do\_snapshot, do\_size\_allocate, etc.)
> 
> There are also things that are a bit more difficult, which I might not be able to solve alone:
> 
> * Dialog execution is now async (save file, etc.) and has to be rewritten
> * Gdk.WindowAttr is gone (is it even needed anymore?)
> * Gtk.RecentChooser is gone, is there a replacement?
> * MeldSourceMap do\_snapshot\_layer not being called, although the new syntax is being used
> * DiffGrid do\_size\_allocate not being called, although the new syntax is being used (which currently prevents the colored overlay being drawn in HandleWindow)
> 
> Help is welcome!
> ![](CEoUYWhaTUCmzENpzphiihBD.png)
> ![](exBydWMKCQjGxecjWrQcRmfO.png)
> ![](ydLJXvQdjsyisMoHreUJCplP.png)

[Diego Povliuk](https://matrix.to/#/@diegopvlk:mozilla.org) announces

> Dosage, an app to track your medications, it's now available on [Flathub](https://flathub.org/apps/io.github.diegopvlk.Dosage). It packs a lot of features like inventory tracking, history, notifications and frequency modes.
> ![](50cc1942fa47169aa5ef75108fdc0fb0d6b87a3a1712139292232384512.png)
> ![](8ee5940e31e3a23b9fafda52a1133047f3de4c411712139265338507264.png)

### Wildcard [↗](https://github.com/fkinoshita/Wildcard)

Test your regular expressions.

[Felipe Kinoshita](https://matrix.to/#/@fkinoshita:gnome.org) says

> Wildcard 0.3.0 is out and available on [Flathub](https://flathub.org/apps/com.felipekinoshita.Wildcard), this update brings a new references panel to quickly test common regex patterns.
> {{< video src="2655a9892c593d6078223ba4217d28dd987c74531711163448601083904.mp4" >}}

### Gir.Core [↗](https://gircore.github.io/)

Gir.Core is a project which aims to provide C# bindings for different GObject based libraries.

[badcel](https://matrix.to/#/@badcel:matrix.org) says

> Gir.Core 0.5.0-preview.2 got released. It is the next step to the upcoming 0.5.0 release and features some Api cleanups, bugfixes and added some more async APIs. See the [release notes](https://github.com/gircore/gir.core/releases/tag/0.5.0-preview.2) for further details.

### Fretboard [↗](https://github.com/bragefuglseth/fretboard)

Look up guitar chords

[Brage Fuglseth](https://matrix.to/#/@bragefuglseth:gnome.org) reports

> This week a couple of nice accessibility improvements landed in Fretboard:
> - Support for both left-handed and right-handed guitar types</li>
> - Independence from text direction for the chord diagram, fixing the mirroring issue it had with right-to-left languages</li>
> - Turkish, Spanish, Norwegian, Portugese and Brazillian Portugese translations, making Fretboard available in a total of 9 languages</li>
> 
> You can get Fretboard on [Flathub](https://flatpak.app/fretboard).
> ![](ad787af405135407f195081c3ac11a73b61cf3651712569202314838016.png)

# Shell Extensions

[somepaulo](https://matrix.to/#/@somepaulo:matrix.org) says

> The Weather Or Not extension (https://extensions.gnome.org/extension/5660/weather-or-not/) has been updated to fix a long-standing bug for Gnome 42-44 users where the indicator would appear twice. A new version has also been published, ingesting backend changes from upstream (Weather O'Clock extension) and refactoring code to work with Gnome Shell 45. For the next release, an options backend is in the works to allow changing the weather indicator placement on the panel.

# GNOME Foundation

[Sonny](https://matrix.to/#/@sonny:gnome.org) says

> The board of directors has created the Internship Committee. It is responsible for promoting, organizing, and conducting internship programs such as Outreachy and Google Summer of Code. 
> 
> https://wiki.gnome.org/InternshipCommittee
> https://gitlab.gnome.org/Teams/internship
> 
> Project proposals should now be submitted to https://gitlab.gnome.org/Teams/internship/project-ideas/

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

