---
title: "#85 Preferred Installations"
author: Felix
date: 2023-03-03
tags: ["phosh", "gnome-builder", "tubeconverter", "denaro"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from February 24 to March 03.<!--more-->

# GNOME Development Tools

### GNOME Builder [↗](https://gitlab.gnome.org/GNOME/gnome-builder)

IDE for writing GNOME-based software.

[hergertme](https://matrix.to/#/@hergertme:gnome.org) reports

> Builder now allows you to choose a preferred Flatpak installation to use when installing new SDKs and SDK extensions. That Flatpak installation must include a remote providing the necessary FlatpakRef to be used.
> ![](6c3e2bb04414e7d6c96513aa297f4eb307fd12e6.png)

# GNOME Circle Apps and Libraries

[Plum Nutty (she/they)](https://matrix.to/#/@plum-nutty:matrix.org) says

> Chess Clock added support for the _Bronstein delay_ and _simple delay_ timing methods.  These methods prevent a player's time from increasing by playing fast moves, as is possible with _increment_ time control.
> ![](uNKAHMFqxNBCFXNbBpvWgTAI.png)

[Sophie 🏳️‍🌈 🏳️‍⚧️ ✊](https://matrix.to/#/@sophieherold:gnome.org) reports

> This week, [Elastic](https://apps.gnome.org/app/app.drey.Elastic/) joined GNOME Circle. Elastic allows you to design spring animations. Congratulations!
> ![](aa42f8d7786a66a6ce117f1c07bdcc3bca15497f.png)

# Third Party Projects

[nxyz](https://matrix.to/#/@nxyz:matrix.org) reports

> This week I released [Conjure](https://github.com/nate-xyz/conjure), a simple gtk4/libadwaita app that allows you to manipulate images with various transform & filter operations. Manipulation is done with the popular image processing library, [ImageMagick](https://imagemagick.org/index.php) with python bindings from [Wand](https://github.com/emcconville/wand).
> 
> gh repo: https://github.com/nate-xyz/conjure
> Flathub page: https://beta.flathub.org/apps/io.github.nate_xyz.Conjure
> ![](GoFbeehDAZONvRWarUlqvHMn.png)

[angeloverlain](https://matrix.to/#/@angeloverlain:matrix.org) announces

> Hello everyone! This week, Sticky Notes was released. It's a simple libadwaita app that allows you to quickly jote down ideas in notes. Notes contain text with some formatting (bold, italic, underline and strikethrough) and every note can be assigned one of 8 pastel colors to categorize notes. You can get it from [Flathub](https://beta.flathub.org/apps/com.vixalien.sticky)
> ![](lRAuzcwmERPjVPBWeOrhXGYm.png)
> ![](zapztXsCVhMzTkePzCfPUXEn.png)

[abb128](https://matrix.to/#/@alexx:tchncs.de) says

> Since my last post, I've updated Live Captions with a few new features:
> * The window can now automatically be kept on top on X11, or on Wayland if you have the GNOME Extension or KWin script
> * Support for lower-end hardware (at potentially reduced accuracy)
> * New history window and history export
> 
> Download [Live Captions from FlatHub](https://flathub.org/apps/details/net.sapples.LiveCaptions) if you're interested in trying it out!
> 
> I've also been working on improving the april-asr library so hopefully more open-source apps can make use of live speech recognition. There are now C# and Python bindings available. I'm not calling them stable yet, but they should be usable. Open an issue if you have any problems or suggestions!

### Tube Converter [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

An easy-to-use video downloader (yt-dlp frontend).

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) announces

> Tube Converter [V2023.3.0-beta1](https://github.com/nlogozzo/NickvisionTubeConverter/releases/tag/2023.3.0-beta1-p1) is here! This is the first beta featuring the new C# rewrite. It continues to use `yt-dlp` and `ffmpeg` in its backend, however, the application now has a much more stable and cleaner architecture, allowing for faster downloads and fewer crashes. The C# rewrite also makes Tube Converter available for Windows! This work couldn't have been possible without the help of @fsobolev and @DaPigGuy ❤️
> 
> Here's the changelog:
> 
> * Tube Converter has been rewritten in C#. With the C# rewrite, Tube Converter is now available on Windows!
> * Added download progress/speed indicators
> * Replaced the View Log button with an Open Save Folder button on successful download and a Retry Download button on error
> * Redesigned download rows to better fit small screens/mobile devices
> * Fixed UI freeze while downloads in progress
> 
> The beta is available on `flathub-beta`:
> 
> ```
> flatpak remote-add --if-not-exists flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
> flatpak install flathub-beta org.nickvision.tubeconverter
> flatpak run org.nickvision.tubeconverter//beta
> ```
> 
> For translators: Please update all [translations on Weblate](https://hosted.weblate.org/projects/nickvision-tube-converter/app/) before Friday, March 3 2023, to be included in the stable release.
> ![](lkNUmQrrnkWveUMKGilTFyST.png)

### Phosh [↗](https://gitlab.gnome.org/World/Phosh/phosh)

A pure wayland shell for mobile devices.

[Guido](https://matrix.to/#/@agx:sigxcpu.org) says

> This week we released phosh 0.25.0 featuring a new plugin to configure the
> emergency preferences as shown on the lock screen by Chris Talbot and a style
> refresh updating the settings menu by Sam Hewitt:
> ![](HTTctKTcJDWAiOwEgBQsZfNg.png)
> ![](MTzujQMdgfuLZimuJMiaDsWa.png)

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

A personal finance manager.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Denaro [V2023.2.2](https://github.com/nlogozzo/NickvisionMoney/releases/tag/2023.2.2) is here! This release includes many UI tweaks and improvements as we gear up for joining *The Circle* ;)
> 
> Here's the full changelog:
> * New and improved icon (Thanks @bertob)!
> * Various UX improvements
> * Updated and added translations (Thanks to everyone on Weblate)!
> ![](IlFNtihhQlIcYVNbqlEcDUyV.png)
> ![](RNjsFWBRpmIBKnQBpPtjHass.png)

# Shell Extensions

[Just Perfection](https://matrix.to/#/@justperfection:gnome.org) says

> The port guide for GNOME Shell 44 extensions is ready:
> https://gjs.guide/extensions/upgrading/gnome-shell-44.html
> If you need any help porting your extensions to GNOME Shell 44, please ask us on [GNOME Matrix Channel](https://matrix.to/#/#extensions:gnome.org)

[Cleo Menezes Jr.](https://matrix.to/#/@cleomenezesjr:matrix.org) announces

> **[Weather O'Clock](https://extensions.gnome.org/extension/5470/weather-oclock/)** and **[Auto Activities](https://extensions.gnome.org/extension/5500/auto-activities/)** extensions received support for GNOME Shell 44.
> Along with their respective releases, there were also some visual refinements and bug fixes.
> ![](UXFZFEbbKWlzUbrBqenrzZbt.png)

# Miscellaneous

[barthalion](https://matrix.to/#/@barthalion:matrix.org) announces

> While my job title proudly says "DevOps Engineer", it really means I'm doing a little bit of everything everywhere. This week has been almost entirely wasted on playing the cat & mouse game with a spammer attacking gitlab.gnome.org. As the part of remediation steps, I ended up disabling external authentication providers like Google, limiting the possibility to create or fork repositories without SSH key configured, and applied on the behalf of GNOME Foundation for [Akismet](https://akismet.com) enterprise plan for better spam detection. I have also started banning such abusive accounts instead of outright removing them, and currently the situation seems to be under control.
> 
> GNOME has been accepted to the [Fast Forward program by Fastly](https://www.fastly.com/fast-forward). Infrastructure changes are not live yet, but we will finally have proper zero downtime upgrades of the underlying servers, as we will no longer rely on DNS-based round-robin traffic distribution, and gain the support for IPv6, including Flathub's website.
> 
> Over at Flathub, we're tying various loose ends related to the upcoming beta version launch. I don't want to spoil too much as we're closer to the finish line than we've ever been since the first commit 2 years ago, but you can already poke around at [beta.flathub.org](https://beta.flathub.org) to see what's coming.

[Hemish 🇮🇳🏳️‍🌈](https://matrix.to/#/@hemish:matrix.org) announces

> New interface translations for Hindi language have been done for GNOME Characters, GNOME Calendar, GNOME Clocks, GNOME Weather, Console, Calls, GNOME Initial Setup, GNOME Tour, GNOME Display Manager (GDM), Sound Recorder, libshumate, gnome-bluetooth, xdg-desktop-portal-gnome, libadwaita, Dialect and Solanum. (Translations did not exist for these software)
> 
> GNOME Shell and Yelp got a lot of previous translations corrected, and new translations added after accumulation of new strings due to being inactive for past 8-9 years. GNOME Shell and Yelp are fully translated now.
> 
> All these translation works are making their way into GNOME 44 release.
> ![](iOyVZekcNdtbQCCGOtOCSkVa.png)

# GNOME Foundation

[Sonny](https://matrix.to/#/@sonny:gnome.org) announces

> GNOME Foundation has been accepted as a GSoC 2023 mentoring org!
> 
> We are glad to announce that once again the GNOME Foundation will be part of [Google Summer of Code](https://summerofcode.withgoogle.com/). We are interested in onboarding new contributors that are passionate about GNOME and motivated to become long term GNOME developers!
> 
> * [list of projects](https://gsoc.gnome.org/2023/)
> * [the announcement](https://discourse.gnome.org/t/gsoc-2023-gnome-foundation-has-been-accepted-as-a-mentoring-org/14214)
> 
> 
> Thank you Felipe Borges for organizing!

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

