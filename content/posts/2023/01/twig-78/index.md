---
title: "#78 Volume Levels"
author: Felix
date: 2023-01-13
tags: ["rnote", "money", "flare", "gaphor", "gnome-control-center", "gdm-settings", "tracker"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from January 06 to January 13.<!--more-->

# Core Apps and Libraries

### Settings [↗](https://gitlab.gnome.org/GNOME/gnome-control-center)

Configure various aspects of your GNOME desktop.

[Marco Melorio](https://matrix.to/#/@melix99:gnome.org) says

> The redesign of the sound panel of GNOME Settings is continuing! This week the "Volume Levels" section has been moved in a separate window, to make the main panel more compact by default. Also, the app icons in the new design are now bigger and full-colored and the rows also include a volume level indicator.
> ![](22eb9f4a4b7928cf2082c1cdb85bcc24d49947a3.png)
> ![](f7d0cf99b5ce0815549f71efd49ab79360de2256.png)

### Tracker [↗](https://gitlab.gnome.org/GNOME/tracker/)

A filesystem indexer, metadata storage system and search tool.

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) says

> Carlos Garnacho added support in the Tracker database library for exporting data as [JSON-LD](https://en.wikipedia.org/wiki/JSON-LD). This will be available via the `tracker3 export` command in Tracker 3.5.
> 
> This feature builds on a series of incremental improvements around data serialization and deserialization in libtracker-sparql. See [MR tracker!523](https://gitlab.gnome.org/GNOME/tracker/-/merge_requests/523) for more details.

# Circle Apps and Libraries

### Gaphor [↗](https://gaphor.org)

A simple UML and SysML modeling tool.

[Arjan](https://matrix.to/#/@amolenaar:matrix.org) reports

> This week Dan Yeaw released [Gaphor](https://gaphor.org) 2.15.0. The improvements include, but are not limited to:
> 
> * Basic git merge conflict support by asking which model to load
> * Improvements to CSS autocomplete for Gaphor's style sheets
> * Native file chooser support in Windows
> * Fixed UTF-8 encoding issues on Windows
> * Fixed translations not loading in Windows, macOS, and AppImage
> 
> Many thanks to everyone who helped out with translating Gaphor. Special thanks for [Jonathan](https://github.com/vanillajonathan) and [Óscar Fernández Díaz](https://github.com/oscfdezdz) for their code contributions.
> ![](NXAZfTiRuuOXMhwdxKvOqiBi.png)

# Third Party Projects

[Paulo](https://matrix.to/#/@raggesilver:matrix.org) reports

> Black Box 0.13.0 is finally out! This release brings much-anticipated features and bug fixes:
> 
> * Customizable keyboard shortcuts with support for fully disabling shortcuts
> * Background transparency
> * Customizable cursor blinking mode
> * Experimental Sixel support (Settings > Advanced > Sixel Support)
> * Fixed scrolling on touchpad and touchscreens
> * Fixed issues with copy/paste
> 
> You can check out the full release notes [here](https://gitlab.gnome.org/raggesilver/blackbox/-/releases/v0.13.0).
> ![](ubShBDQWiVupBhFpEfXwVUHc.png)

[Romain](https://matrix.to/#/@romainvigier:matrix.org) announces

> I updated [UI Shooter](https://gitlab.com/rmnvgr/uishooter), my tool to take screenshots of GTK4 widgets, used for example in CI to take screenshots in different languages for documentation. The new release correctly sets the direction of widgets in right-to-left languages, and the [container image](https://gitlab.com/rmnvgr/uishooter/container_registry/2639982) now includes all langpacks fonts.

### Rnote [↗](https://github.com/flxzt/rnote)

Sketch and take handwritten notes.

[flxzt](https://matrix.to/#/@flxzt:matrix.org) says

> I just merged the `tabs` branch into `main` in the Rnote repository. Thanks to the awesome "TabView" libadwaita widget the tabs themselves were really easy to implement.  Alongside with it I also implemented a global colorpicker, and made the UI more pretty with floating toolbars. There are still some things left to do and likely bugs to squash, but I am really excited to get this out as a beta release soon.
> ![](qinMQajFQvFDuttTxTFzAsBh.png)

### Denaro [↗](https://flathub.org/apps/details/org.nickvision.money)

A personal finance manager.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) reports

> Money has been renamed to Denaro!
> As many of you have seen, Money has been rewritten in C# and has many new features. To further signify this great new release, Money has been renamed to Denaro! Although there is a new name, the code will continue to use `NickvisionMoney` for namespaces and `.nmoney` for files. All `.nmoney` files from previous stable Money releases are 100% compatible with Denaro.
> 
> Denaro [V2023.1.0-rc1](https://github.com/nlogozzo/NickvisionMoney/releases/tag/2023.1.0-rc1) was also released! Compared to previous betas:
> 
> * We implemented a completely new Account Settings dialog that allows users to further customize their Denaro accounts (including support for a custom currency).
> * We also implemented the Export to PDF feature, which generates a beautiful overview report of your account with groups, transactions, and receipts all included.
> * Documentation for the app is also available, built using yelp-tools which is available through the Help menu item on both GNOME and WinUI!
> 
> We hoped to release the stable version today, but we found a memory leak that we are investigating and are trying to fix. New planned release day is Sunday :)
> ![](DilKoOxAeNbOBdQvxJBdpZAH.png)
> ![](hAeaNhcOGxofYbsugdgctIcn.png)

### Login Manager Settings [↗](https://realmazharhussain.github.io/gdm-settings/)

A settings app for the login manager GDM.

[Mazhar Hussain](https://matrix.to/#/@realmazharhussain:matrix.org) reports

> [gdm-tools](https://github.com/realmazharhussain/gdm-tools) [v1.2](https://github.com/realmazharhussain/gdm-tools/releases/tag/v1.2) has been released.
> 
> It is a boring bug fix release improving the install/uninstall experience. The only notable new feature is a `--no-ask` option to the install script which enables hands-free/non-interactive install process.
> 
> I still recommend [Login Manager Settings](https://realmazharhussain.github.io/gdm-settings) over this but for the people who want to use gdm-tools, the install/uninstall process should be better now.

### Flare [↗](https://flathub.org/apps/details/de.schmidhuberj.Flare)

A unofficial Signal GTK client.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) reports

> Flare version 0.6.0 has been released. In the last few months, there were not too many updates happening with Flare, but many bugs were fixed that leads to Flare being more stable and usable. In total, in the last three months, there were seven minor releases of Flare fixing bugs.
> 
> This week, Flare version 0.6.0 has been released with the major addition for notifications in the background. This means that (once the option is enabled in the settings) Flare will be able to start in the background and send message notifications without it being manually opened by the user. Furthermore, there has been some work done on restarting Flare after waking up from suspension, which is great for usage in Linux-based phones that regularly enter and exit suspension. This addition was mainly implemented by Cédric Bellegarde.
> 
> In additions to these major features, a [workaround](https://gitlab.com/Schmiddiii/flare/-/issues/4#note_1234642176) has been found for the [notorious issue](https://gitlab.com/Schmiddiii/flare/-/issues/4) where Flare would not work with libsecret on some distributions. If you previously had that problem with Flare, applying that workaround should make Flare usable on your distribution. Furthermore, the summary of Flare has received a minor update to correct a grammatical mistake that was present for over six months.

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

