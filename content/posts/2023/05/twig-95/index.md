---
title: "#95 Overlaid Zoom"
author: Felix
date: 2023-05-12
tags: ["flare", "gnome-maps", "metronome", "tubeconverter"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from May 05 to May 12.<!--more-->

# GNOME Core Apps and Libraries

### Maps [↗](https://wiki.gnome.org/Apps/Maps)

Maps gives you quick access to maps all across the world.

[mlundblad](https://matrix.to/#/@mlundblad:matrix.org) reports

> Maps now has moved the zoom control buttons back to use overlay buttons (they were moved to the headerbar to work around issues with Clutter and GTK overlays in earliear versions when using Clutter), additionally there is a button to indicate map rotation, with the ability to click it to reset to normal "north is up". Useful on touch screens when using gestures to rotate. Additionally there's now keyboard shortcuts to rotate, and reset rotation
> ![](pUjhaWTVpLQfwiDmVzwvxSMN.png)

# GNOME Circle Apps and Libraries

### Metronome [↗](https://gitlab.gnome.org/aplazas/metronome)

Practice music with a regular tempo.

[Clara Hobbs (she/they)](https://matrix.to/#/@plum-nutty:matrix.org) reports

> Metronome 1.2.1 was released! This new release updates the Flatpak runtime to GNOME 44, finally bringing support for the dark style.  It is now possible to toggle the metronome using the spacebar, regardless what GUI element is selected.  We also landed some minor UI improvements, bringing the app more in line with the HIG.
> ![](BjDKDAuXfwbGrVKZVTKEQlrZ.png)

# Third Party Projects

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> [Icon Library](https://flathub.org/apps/org.gnome.design.IconLibrary) finally has the most requested feature: Displaying the license of the icons and a lot of new icons brought to you by the design team.
> ![](78115c8b0f73cca79a4d5e8c6833862df841a050.png)
> ![](14b0b17d34869b8203aefd4509a7bfd34fce3fac.png)

> I have published a new release of [Symbolic Preview](https://flathub.org/apps/org.gnome.design.SymbolicPreview) featuring progressive icons rendering. The old release used to block the UI until all the icons were rendered.
> {{< video src="1816e7e584abbdac480b19520f83b8efd64a0460.webm" >}}

[gregorni](https://matrix.to/#/@gregorni:hackliberty.org) announces

> This week, I released version 1.3.0 of Letterpress (previously ASCII Images) on [Flathub](https://flathub.org/apps/io.gitlab.gregorni.ASCIIImages)! The new release features a better warning dialog when the output is too big, the "Output Width" setting's subtitle is now more adaptive, and the buttons for copying and saving the output have been swapped. As part of the rebrand, the welcome page now shows a nice illustration (shoutout to Brage, who also came up with the new name) instead of the app's symbolic icon. Finally, Czech and Turkish translations have been added as well.
> ![](qWWeJMcdDzcxuBJcUjVBJICz.png)
> ![](iMCjjEeqmuTAZKQdeGjleIxa.png)
> ![](gdPXFpHPDNKflxvzQymERSol.png)

[0xMRTT](https://matrix.to/#/@0xmrtt:envs.net) announces

> Imaginer has been released! Imaginer is a new application for generating pictures with various AI.
> 
> You can download Imaginer from [Flathub](https://flathub.org/apps/page.codeberg.Imaginer.Imaginer) or from either [Github](https://github.com/ImaginerApp/Imaginer) or [Codeberg](https://codeberg.org/Imaginer/Imaginer)
> ![](cd221aee84320e331abcac9fc5e1b1756139154d.png)

> Bavarder 0.2.0 has been released with support for a formatted output (table/code) and a new website with documentation for explaining how to get a token.
> ![](b38358c541d9308e0608476fa1d62d65161a3097.png)
> ![](f68977be51f7838aa0c0187a56ccc55383830853.png)

[Michael Terry](https://matrix.to/#/@mterry:gnome.org) announces

> [Multiplication Puzzle](https://flathub.org/apps/app.drey.MultiplicationPuzzle) 12.0 released with some UI tweaks (uses an AdwBanner now for the victory screen) and it also changed appids, which flatpak should prompt you about when updating

### Tube Converter [↗](https://flathub.org/apps/details/org.nickvision.tubeconverter)

Get video and audio from the web.

[Nick](https://matrix.to/#/@nlogozzo:matrix.org) says

> Tube Converter received [V2023.5.0-beta1](https://github.com/NickvisionApps/TubeConverter/releases/tag/2023.5.0-beta1) this week! 
> 
> This update includes the ability to stop all downloads, retry all failed downloads, and clear all downloads that you may have in your queue (previously these actions had to happen on a per download basis). 
> 
> Besides that, we completely rewrote the backend for managing downloads, which should now fix many of the crashing issues users were experiencing :)
> ![](LfxdEGjPbeeruRKuZAOUJTKS.png)

### Flare [↗](https://flathub.org/apps/details/de.schmidhuberj.Flare)

An unofficial Signal GTK client.

[schmiddi](https://matrix.to/#/@schmiddi:matrix.org) says

> Flare 0.8.1 was release with many UI and UX improvements including some style changes to the message list to better differentiate own messages from messages sent by others. Furthermore, channels that don't have messages yet are now hidden by default, one now has to explicitly show them using the new "compose"-button. The final noteworthy addition is a new button to scroll down in the list view.
> ![](lGTCzGjfBKeaVKePLpwvYhrO.png)

# Miscellaneous

[Sam Thursfield](https://matrix.to/#/@ssssam:matrix.org) says

> There are improvements to the GNOME openQA tests:
> 
> * The tests are now split into multiple testsuites. Now we can develop multiple sets of tests and run them independently ([issue 26](https://gitlab.gnome.org/GNOME/openqa-tests/-/issues/26))
> * A commandline tool named `pipeline_report.py` was merged to make it easier to analyse test failures ([MR 25](https://gitlab.gnome.org/GNOME/openqa-tests/-/merge_requests/25))  
> * The individual openQA test results are now shown in the Gitlab merge request UI ([issue 32](https://gitlab.gnome.org/GNOME/openqa-tests/-/issues/32) - screenshot below)
> * Most importantly, we are about to run test #1000 at https://openqa.gnome.org/
> ![](zyzxhUPUXCqKDWWzYvTrCkXa.png)

# Google Summer of Code

[feborges](https://matrix.to/#/@felipeborges:gnome.org) reports

> GNOME will be mentoring 9 new contributors in Google Summer of Code 2023!
> 
> GSoC is a program focused on bringing new contributors into open source software development. A number of long term GNOME developers are former GSoC interns, making the program a very valuable entry point for new members in our project.
> 
> The new contributors will soon get their blogs added to [Planet GNOME](https://planet.gnome.org), making it easy for the GNOME community to get to know them and the projects that they will be working on.
> 
> https://discourse.gnome.org/t/announcement-gnome-will-be-mentoring-9-new-contributors-in-google-summer-of-code-2023/15232

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

