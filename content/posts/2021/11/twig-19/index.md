---
title: "#19 Updated Calculations"
author: Felix
date: 2021-11-19
tags: ["gnome-calculator", "fragments", "gnome-shell", "kgx", "libadwaita", "junction", "gnome-software", "gnome-calls", "glib", "gjs", "crosswords", "gnome-sound-recorder"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from November 12 to November 19.<!--more-->

# Core Apps and Libraries

### Calculator [↗](https://wiki.gnome.org/Apps/Calculator)

Perform arithmetic, scientific or financial calculations.

[Chris 🌱️](https://matrix.to/#/@brainblasted:gnome.org) reports

> Robert Roth and I have ported Calculator to GTK4+libadwaita. If you'd like to test it early, you can now install it via the GNOME nightly flatpak repository.
> ![](61e110be270c181eff07ba28aad07ac369fee021.png)

### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> `AdwLeaflet` now supports shortcuts and mouse back/forward buttons in addition to touch and touchpad swipes for navigating back/forward. The corresponding properties has been renamed from `can-swipe-back/forward` to `can-navigate-back/forward` to reflect that.

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Milan Crha [added support for libsoup3 to gnome-software](https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/1091). Many components are currently being ported to libsoup3 by a variety of contributors across the desktop.

### GNOME Shell [↗](https://gitlab.gnome.org/GNOME/gnome-shell)

Core system user interface for things like launching apps, switching windows, system search, and more.

[Ivan Molodetskikh](https://matrix.to/#/@yalter:gnome.org) announces

> This week I improved the way windows are laid out in the window selection mode of the work-in-progress [screenshot UI](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/1954). Like in the regular overview, client-side window shadows are no longer included into the window size. Selection is now indicated with a nice rounded outline similar to the one in GNOME 3.38. Finally, I removed the background wallpaper to reduce confusion between the screenshot UI and the overview.
> {{< video src="5708d91bc1d1f870b7544813bf4ebd6a6ca0823e.webm" >}}

### Calls [↗](https://gitlab.gnome.org/GNOME/calls)

A phone dialer and call handler.

[Evangelos](https://matrix.to/#/@evangelos.tzaras:talk.puri.sm) announces

> I worked with Guido on showing a contact's [avatars](https://gitlab.gnome.org/World/Phosh/libcall-ui/-/merge_requests/17) in [Calls](https://gitlab.gnome.org/GNOME/calls/-/merge_requests/449) using libhandy's `HdyAvatar`. 
> Avatar information is exposed over [DBus](https://gitlab.gnome.org/GNOME/calls/-/merge_requests/450) allowing [Phosh](https://gitlab.gnome.org/World/Phosh/phosh/-/merge_requests/943) to show avatars on the lockscreen.
> ![](qsrXXCMYjcFenDNXnYmofSVc.png)
> ![](mxGRXsPZnAjkzuFFixGSwPzR.png)

### GLib [↗](https://gitlab.gnome.org/GNOME/glib)

The low-level core library that forms the basis for projects such as GTK and GNOME.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> Emmanuel Fleury has [almost finished his campaign](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/2323) of fixing warnings in the Windows code in GLib, which will allow us to keep code quality up more easily in future.

### GJS [↗](https://gitlab.gnome.org/GNOME/gjs)

Use the GNOME platform libraries in your JavaScript programs. GJS powers GNOME Shell, Polari, GNOME Documents, and many other apps.

[Philip Withnall](https://matrix.to/#/@pwithnall:matrix.org) announces

> In GJS, Evan Welsh fixed [issue #1](https://gitlab.gnome.org/GNOME/gjs/-/issues/1) after a lot of careful work. It should make the performance of promises in JS more predictable under higher load.

# Circle Apps and Libraries

### Fragments [↗](https://gitlab.gnome.org/World/Fragments)

Easy to use BitTorrent client.

[Felix](https://matrix.to/#/@felix:haecker.io) says

> Fragments V2 has made good progress in the past few days. Maximiliano and I implemented the necessary bits in [transmission-client](https://gitlab.gnome.org/haecker-felix/transmission-client/-/merge_requests/2) and [transmission-gobject](https://gitlab.gnome.org/haecker-felix/transmission-gobject/-/merge_requests/3) to allow changing the settings of the underlying transmission-daemon. The new [redesigned preferences window](https://gitlab.gnome.org/World/Fragments/-/merge_requests/108) added support for many requested settings, like selecting an own folder for incomplete torrents. Maximiliano also [ported the in-app notifications](https://gitlab.gnome.org/World/Fragments/-/merge_requests/124) to the new `AdwToast` API. Chris 🌱️ added support for [opening downloaded torrents](https://gitlab.gnome.org/World/Fragments/-/merge_requests/123).
> ![](xcMXAVvwPQjTXwYCLCeaocRv.png)

# Third Party Projects

### KGX [↗](https://gitlab.gnome.org/ZanderBrown/kgx)

A simple user-friendly terminal emulator

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) reports

> Chris 🌱️ has added a [light/dark switcher](https://gitlab.gnome.org/ZanderBrown/kgx/-/merge_requests/63) and the app now always [uses](https://gitlab.gnome.org/ZanderBrown/kgx/-/merge_requests/65) "Terminal" branding instead of "King's Cross"
> ![](e77ab9f06ce492269fa261d6026fa3f825112bb2.png)

### Junction [↗](https://github.com/sonnyp/Junction)

Lets you choose the application to open files and links.

[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) says

> [Junction](https://github.com/sonnyp/Junction) 1.2.0 has been released with better compatibility, new features and a better design thanks to libadwaita and Tobias Bernard's help.
> ![](WnaTJzbUzlKPDVqNBwGxWpjw.png)

### Sound Recorder [↗](https://wiki.gnome.org/Apps/SoundRecorder)

A simple, modern sound recorder.

[Bilal Elmoussaoui](https://matrix.to/#/@bilelmoussaoui:gnome.org) announces

> Chris 🌱️'s port of Sound Recorder to GTK 4 / libadwaita has been cleaned up and merged today.
> ![](f5610b89eb28bbebdc8330169d7a047e71fc33bd.png)

### Crosswords [↗](https://gitlab.gnome.org/jrb/crosswords)

A simple Crossword player and Editor.

[Jonathan Blandford](https://matrix.to/#/@jblandford:matrix.org) reports

> An initial version of GNOME Crosswords was released and is looking for feedback. This is a simple crossword game for GNOME, with a planned Crossword Editor in the works. See the [release announcement](https://blogs.gnome.org/jrb/2021/11/18/introducing-gnome-crosswords/) for more details.
> ![](HqAHXhNtEsHlbtdlFGjZxYPU.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
