---
title: "#10 Making History"
author: Felix
date: 2021-09-17
tags: ["dejadup", "fractal", "dialect", "telegrand", "emblem"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from September 10 to September 17.<!--more-->

# Third Party Projects

### Fractal [↗](https://gitlab.gnome.org/GNOME/fractal)

Matrix messaging app for GNOME written in Rust.

[Alexandre Franke](https://matrix.to/#/@afranke:matrix.org) says

> Big news in Fractal this week: [history loading landed](https://gitlab.gnome.org/GNOME/fractal/-/merge_requests/826), thanks to Julian.

### Telegrand [↗](https://github.com/melix99/telegrand/)

A Telegram client optimized for the GNOME desktop.

[Marco Melorio](https://matrix.to/#/@melix99:gnome.org) reports

> Telegrand has seen good improvements over this week! The outgoing messages [now use the accent color](https://github.com/melix99/telegrand/pull/79) (helpful to better identify own messages). Incoming messages have been made slightly lighter.
> 
> I added the support to show stickers and some events-related messages (like deleting the group or channel photo) in the chat.
> ![](7fa56f8f4334844d923b359a709b4096276a30ff.png)

### Emblem [↗](https://gitlab.gnome.org/World/design/emblem/)

[Maximiliano](https://matrix.to/#/@msandova:gnome.org) announces

> Introducing Emblem, a new design tool to generate project avatars for GitLab, GitHub and your Matrix room.
> 
> You can get Emblem at [flathub](https://flathub.org/apps/details/org.gnome.design.Emblem),  you can also read the accompanying [blog post](https://blogs.gnome.org/msandova/2021/09/15/introducing-emblem/).
> ![](9cac1b57b2cc938b500f44a3b1dc81b37532c973.png)

# Circle Apps and Libraries

[Sophie Herold](https://matrix.to/#/@sophieherold:gnome.org) says

> This week [Share Preview](https://apps.gnome.org/app/com.rafaelmardojai.SharePreview/) entered GNOME Circle. Share Preview allows to quickly preview the appearance of page links on social media. Congratulations!
> ![](dc2895c3bca9259eb14921ea57f0e754d1b61db6.png)

### Dialect [↗](https://github.com/dialect-app/dialect/)

Translate between languages.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) reports

> Dialect now has [localized language names](https://github.com/dialect-app/dialect/pull/200), so now you can see 100% of the UI in your language! Translators don't need to worry, those name can be generated automagically.
> ![](ZhVzxghzmkrDCrrzypeCJxuz.png)

### Déjà Dup Backups [↗](https://wiki.gnome.org/Apps/DejaDup/)

A simple backup tool.

[Michael Terry](https://matrix.to/#/@mterry:gnome.org) says

> Déjà Dup got a small redesign to its default overview page, partly to allow the window to be resized much smaller but also to look a little more like standard GNOME design.
> ![](44ced2a7b3151678797e29ef5abb857e8d0acfcb.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!

