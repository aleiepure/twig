---
title: "#3 Documentation and Libraries"
author: Thib
date: 2021-07-30
tags: ["gnome-maps", "gnome-text-editor", "connections", "kooha", "gnome-software", "blanket", "libadwaita", "drawing"]
categories: ["twig"]
draft: false
---

Update on what happened across the GNOME project in the week from July 23 to July 30.<!--more-->

# Core Apps and Libraries
### Libadwaita [↗](https://gitlab.gnome.org/GNOME/libadwaita)

Building blocks for modern GNOME apps using GTK4.

[Alexander Mikhaylenko](https://matrix.to/#/@alexm:gnome.org) says

> libadwaita now [supports](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/208) `.flat` style class for header bars, as used in applications like [Solanum](https://flathub.org/apps/details/org.gnome.Solanum) or [Breathing](https://flathub.org/apps/details/io.github.seadve.Breathing)
> 
> ![](libadwaita_flat.png)

[Tobias Bernard](https://matrix.to/#/@tbernard:gnome.org) says

> Libadwaita window shadows are now softer and way nicer looking ([merge request](https://gitlab.gnome.org/GNOME/libadwaita/-/merge_requests/182)).
> 
> ![](libadwaita_shadows.png)

### Text Editor [↗](https://gitlab.gnome.org/GNOME/gnome-text-editor/)

Text Editor is a simple text editor that focus on session management.

[Allan Day](https://matrix.to/#/@aday:gnome.org) says

> In Text Editor, Christian Hergert changed the preferences to a sidebar, and added a style schema option
> * https://gitlab.gnome.org/GNOME/gnome-text-editor/-/commit/b413feb1acf54e6bbc802d708a9a5eb00d44ea1f
> * https://gitlab.gnome.org/GNOME/gnome-text-editor/-/commit/6fd8d4b47f39a7bd7689c54f544ce26384d1ff57
> * https://gitlab.gnome.org/GNOME/gnome-text-editor/-/commit/bc86cac531cc86d0c35162a42bfb77610dfc83a3

[Allan Day](https://matrix.to/#/@aday:gnome.org) announces

> Also in Text Editor, Christian did a round of bug fixing and added keyboard shortcuts for zooming - https://gitlab.gnome.org/GNOME/gnome-text-editor/-/commit/ed44e9b08565421d9234e2d7e0844d35ce24d40c

### Software [↗](https://gitlab.gnome.org/GNOME/gnome-software/)

Lets you install and update applications and system extensions.

[Allan Day](https://matrix.to/#/@aday:gnome.org) reports

> Adrien Plazas and Jakub Steiner added a new icon set for system components - https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/868

[Allan Day](https://matrix.to/#/@aday:gnome.org) announces

> This week in Software:
>    - Adrien Plazas cleaned up the updates list and made it adaptive - https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/855
>    - Adrien also gave the preferences window a modern style - https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/860
>    - Milan Crha fixed a bug where the launch button took a long time to appear after app installation - https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/857

[Adrien Plazas](https://matrix.to/#/@adrien.plazas:gnome.org) announces

> I made application screenshots scale to the window's width - https://gitlab.gnome.org/GNOME/gnome-software/-/merge_requests/867

### Maps [↗](https://wiki.gnome.org/Apps/Maps)

Maps gives you quick access to maps all across the world.

[Corentin Noël](https://matrix.to/#/@tintou:matrix.org) says

> In libshumate, James Westman implemented support for rotating a Map: https://gitlab.gnome.org/GNOME/libshumate/-/commit/c54ed6c66b72a2d8016319c5c991a4c6a506109c

### Connections [↗](https://gitlab.gnome.org/GNOME/connections/)

A remote desktop client.

[Allan Day](https://matrix.to/#/@aday:gnome.org) announces

> Felipe Borges landed various improvements to the Connections user experience:
> * Made it possible to connect and switch to multiple connections at the same time - https://gitlab.gnome.org/GNOME/connections/-/merge_requests/78
> * Added error messages and error handling when connecting - https://gitlab.gnome.org/GNOME/connections/-/merge_requests/79
> * Improved the design of the new connection popover - https://gitlab.gnome.org/GNOME/connections/-/merge_requests/77

# Documentation
[Emmanuele Bassi](https://matrix.to/#/@ebassi:gnome.org) says

> The new developer documentation website is up at [developer.gnome.org](https://developer.gnome.org/). The old API references are available at [developer-old.gnome.org](https://developer-old.gnome.org).
> 
> ![](gnome_dev_doc.png)

# Circle Apps and Libraries
[Martín Abente Lahaye](https://matrix.to/#/@tchx84:matrix.org) reports

> [GNOME Circle](https://circle.gnome.org/) accepted two new apps this week. We're happy to welcome [Podcasts](https://flathub.org/apps/details/org.gnome.Podcasts) and [Wike](https://flathub.org/apps/details/com.github.hugolabe.Wike)!

### Kooha [↗](https://github.com/SeaDve/Kooha)

A simple screen recorder with a minimal interface. You can simply click the record button without having to configure a bunch of settings.

[SeaDve](https://matrix.to/#/@sedve:matrix.org) announces

> GIF and MP4 video formats support has been added in [Kooha](https://github.com/SeaDve/Kooha). Recording with GIF doesn't require post-processing, which makes it much more efficient than a typical GIF recorder.

### Drawing [↗](https://github.com/maoschanz/drawing)

A basic image editor, similar to Microsoft Paint, but aiming at the GNOME desktop.

[maoschannz](https://matrix.to/#/@maoschannz:matrix.org) announces

> A new tool, "Skew", has been added to [Drawing](https://github.com/maoschanz/drawing). It allows you to tilt the selection horizontally or vertically.
> 
> ![](drawing.png)

### Blanket [↗](https://github.com/rafaelmardojai/blanket)

Improve focus and increase your productivity by listening to different sounds.

[Rafael Mardojai CM](https://matrix.to/#/@rafaelmardojai:matrix.org) announces

> In Blanket is now possible to [toggle sounds faster by pressing the sound row](https://github.com/rafaelmardojai/blanket/commit/c0abfead5ce2068e6a856f41b7274d07d3f27d9d). The volume level will be set to 50% when activated.

# Third Party Projects
[sonnyp](https://matrix.to/#/@sonnyp:matrix.org) says

> [Oh My SVG](https://github.com/sonnyp/OhMySVG/) is a new application to optimize SVGs
>
> ![](oh-my-svg.png)

# That’s all for this week!

See you next week, and be sure to stop by [#thisweek:gnome.org](https://matrix.to/#/#thisweek:gnome.org) with updates on your own projects!
